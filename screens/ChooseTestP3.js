import React, { Component } from 'react';
import { Text, StyleSheet, Image, Dimensions, TouchableOpacity, View, Alert } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Header, Title, Input, Container, Content, Footer, ListItem, List, CheckBox, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import PlanMeFont from '../Theme/PlanMeFont';

var { height, width } = Dimensions.get('window');

export default class ChooseTestP2 extends Component {
	constructor() {
		super();
		this.handleList = this.handleList.bind(this);
	}

	handleList(to, from) {
		let disciplinas = this.props.store.disciplinasSelecionadas;
		if (from >= 0 && from <= disciplinas.length - 1) {
			let temp = disciplinas[from];
			disciplinas[from] = disciplinas[to];
			disciplinas[to] = temp;
			this.props.store.inputData('disciplinasSelecionadas', disciplinas);
		}
	}

	render() {
		let tempArr = this.props.store.disciplinasSelecionadas;
		let listNode = tempArr.map((d, x) => <ItemChallenge key={x} index={x} disciplina={d} handleList={this.handleList} length={tempArr.length} />);

		return (
			<Container>
				<Header style={style.header}>
					<Image style={style.img} source={require('../image/logo.png')} />
				</Header>
				<Content style={style.content}>
					<Text style={style.subtitle}>Confirme as disciplinas e{'\n'}defina a ordem de estudo?</Text>
					<List ref='list'>
						{listNode}
					</List>
				</Content>
				<Footer style={{ backgroundColor: 'transparent', height: 80, paddingTop: 10 }}>
					<Grid style={{ top: -40 }}>
						<Col>
							<Row style={[style.arrow, { marginRight: 1.5 }]}>
								<View style={style.triangleLeft}></View>
								<TouchableOpacity style={[style.buttom, { backgroundColor: '#CCCFD1', left: -7 }]} onPress={Actions.chooseTest2}>
									<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>VOLTAR</Text>
								</TouchableOpacity>
							</Row>
						</Col>
						<Col>
							<Row style={[style.arrow, { marginLeft: 1.5 }]}>
								<TouchableOpacity style={[style.buttom, { left: 7 }]} onPress={Actions.resumePlan}>
									<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>AVANÇAR</Text>
								</TouchableOpacity>
								<View style={style.triangleRight}></View>
							</Row>
						</Col>
					</Grid>
				</Footer>
			</Container>
		);
	}
}

class ItemChallenge extends Component {
	render() {
		return (
			<ListItem style={style.item} onPress={() => { }}>
				<Row>
					<Col size={10}>
						<Button style={{ backgroundColor: this.props.index === 0 ? '#CCC' : '#3EE47B' }} onPress={() => this.props.handleList(this.props.index, this.props.index - 1)}>
							<PlanMeFont color='#FFF' size={18} name='cima' />
						</Button>
					</Col>
					<Col size={80}>
						<Text style={{ marginLeft: 5, color: '#046AF0', fontFamily: 'Montserrat-Regular' }}>{this.props.disciplina[Object.keys(this.props.disciplina)[0]]}</Text>
					</Col>
					<Col size={10}>
						<Button style={{ backgroundColor: this.props.index === this.props.length - 1 ? '#CCC' : '#9D53B8' }} onPress={() => this.props.handleList(this.props.index, this.props.index + 1)}>
							<PlanMeFont color='#FFF' size={18} name='baixo' />
						</Button>
					</Col>
				</Row>
			</ListItem>
		);
	}
}

const style = StyleSheet.create({
	header: {
		backgroundColor: '#fff',
		justifyContent: 'center'
	},
	img: {
		width: 80,
		height: 43
	},
	content: { backgroundColor: '#fff' },
	subtitle: {
		textAlign: 'left',
		flexWrap: 'wrap',
		lineHeight: 30,
		fontSize: 20,
		color: '#9D53B8',
		paddingLeft: 35,
		fontFamily: 'Montserrat-Regular',
		marginTop: 30
	},
	buttom: {
		flex: 1,
		backgroundColor: '#3EE47B',
		marginTop: 40,
		borderRadius: 5,
		height: 46,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	triangleLeft: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 22,
		borderRightWidth: 22,
		borderBottomWidth: 20,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#CCCFD1',
		transform: [{ rotate: '-90deg' }],
		top: 53,
		left: 6
	},
	triangleRight: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 22,
		borderRightWidth: 22,
		borderBottomWidth: 20,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#3EE47B',
		transform: [{ rotate: '90deg' }],
		top: 53,
		right: 6
	},
	arrow: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center'
	},
	item: {
		justifyContent: 'center'
	}
});