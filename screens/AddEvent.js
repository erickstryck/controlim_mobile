import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Grid, Row, Col, Text, Button, InputGroup, Input, Content } from 'native-base';
import { Actions } from 'react-native-mobx';
import PlanMeFont from '../Theme/PlanMeFont';
import CommomPage from './CommomPage';

export default class AddEvent extends Component {

  render() {
    let rightButton = <Button transparent style={{ width: 50 }}><PlanMeFont name='certo' style={{ color: '#FFF', fontSize: 23 }} /></Button>
    let leftButton = <Button transparent style={{ width: 50 }} onPress={Actions.calendar}><PlanMeFont name='anterior' style={{ color: '#FFF', fontSize: 23 }} /></Button>
    return (
      <CommomPage title='NOVO EVENTO' page='calendar' leftButton={leftButton} rightButton={rightButton}>
        <Content>
          <Grid style={{ backgroundColor: '#F2F3F3' }}>
            <Row style={[styles.border, styles.white, styles.row]}>
              <Col>
                <Row>
                  <Col>
                    <InputGroup style={styles.border} borderType='none'>
                      <Input style={styles.input} placeholder='Título' placeholderTextColor='#046AF0' />
                    </InputGroup>
                  </Col>
                </Row>
                <Row style={styles.margin}>
                  <Col>
                    <Row>
                      <Col style={styles.icon} size={20}>
                        <PlanMeFont color='#646C73' size={20} name='pin' />
                      </Col>
                      <Col size={80}>
                        <InputGroup style={{ borderBottomColor: '#FFF' }} borderType='none'>
                          <Input style={styles.input} placeholder='Local' placeholderTextColor='#046AF0' />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row style={[styles.border, styles.white, styles.row]}>
              <Col>
                <Row>
                  <Col>
                    <Row>
                      <Col style={styles.icon} size={20}>
                        <PlanMeFont color='#646C73' size={20} name='tempo' />
                      </Col>
                      <Col style={styles.border} size={80}>
                        <InputGroup style={{ borderBottomColor: '#FFF' }} borderType='none'>
                          <Input style={styles.input} placeholder='Início' placeholderTextColor='#046AF0' />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row style={styles.margin}>
                  <Col>
                    <Row>
                      <Col style={styles.icon} size={20} />
                      <Col style={styles.border} size={80}>
                        <InputGroup style={{ borderBottomColor: '#FFF' }} borderType='none'>
                          <Input style={styles.input} placeholder='Fim' placeholderTextColor='#046AF0' />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row style={styles.margin}>
                  <Col>
                    <Row>
                      <Col style={styles.icon} size={20} />
                      <Col style={styles.border} size={80}>
                        <InputGroup style={{ borderBottomColor: '#FFF' }} borderType='none'>
                          <Input style={styles.input} placeholder='Repetir' placeholderTextColor='#046AF0' />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row style={styles.margin}>
                  <Col>
                    <Row>
                      <Col style={styles.icon} size={20}>
                        <PlanMeFont color='#646C73' size={20} name='relogio' />
                      </Col>
                      <Col size={80}>
                        <InputGroup style={{ borderBottomColor: '#FFF' }} borderType='none'>
                          <Input style={styles.input} placeholder='Alerta' placeholderTextColor='#046AF0' />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row style={[styles.white, styles.row]}>
              <Col>
                <Row style={styles.margin}>
                  <Col>
                    <Row>
                      <Col style={styles.icon} size={20}>
                        <PlanMeFont color='#646C73' size={20} name='papel' />
                      </Col>
                      <Col style={styles.border} size={80}>
                        <InputGroup style={{ borderBottomColor: '#FFF' }} borderType='none'>
                          <Input style={styles.input} placeholder='Notas' placeholderTextColor='#046AF0' />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        </Content>
      </CommomPage>
    );
  }
}

const styles = StyleSheet.create({
  white: {
    backgroundColor: '#FFF', marginTop: 30
  },
  margin: {
    marginTop: 15
  },
  border: {
    borderBottomWidth: 1, borderBottomColor: '#A1A7AA'
  },
  row: {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 25,
    paddingRight: 25
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
    color: '#046AF0'
  }
});