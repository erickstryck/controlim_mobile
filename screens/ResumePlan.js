import React, { Component } from 'react';
import { Text, StyleSheet, Image, View, Dimensions, Platform } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Header, Title, Container, Content, Footer, Button } from 'native-base';
import PlanMeFont from '../Theme/PlanMeFont';
import moment from 'moment';
import { Actions } from 'react-native-mobx';
import BridgeNative from '../controller/BridgeNative';

import TimePlan from '../util/TimePlan'

const div = 15;
var { height, width } = Dimensions.get('window');

export default class ResumePlan extends Component {
	constructor() {
		super();
		this.renderCalendar = this.renderCalendar.bind(this);
		this.findMinTime = this.findMinTime.bind(this);
		this.getMinHistoryTime = this.getMinHistoryTime.bind(this);
		this.makeFragment = this.makeFragment.bind(this);
		this.historyProcess = this.historyProcess.bind(this);
		this.checkBlankSpace = this.checkBlankSpace.bind(this);
	}

	renderCalendar() {
		/*array de dados*/
		var data = TimePlan.calcularTempo(this.props.store.configuracoesTempo);
		// data = [
		// 	{ dia: 'D', horarios: [{ hora: '08:00', cor: '#046AF0', duration: 60 }, { hora: '09:30', cor: '#3EE47B', duration: 30 }, { hora: '10:15', cor: '#EB7A00', duration: 60 }, { hora: '11:15', cor: '#9D53B8', duration: 60 }] },
		// 	{ dia: 'S', horarios: [{ hora: '08:00', cor: '#046AF0', duration: 60 }, { hora: '09:00', cor: '#3EE47B', duration: 60 }, { hora: '10:15', cor: '#EB7A00', duration: 60 }, { hora: '11:15', cor: '#9D53B8', duration: 60 }] },
		// 	{ dia: 'T', horarios: [{ hora: '08:00', cor: '#046AF0', duration: 60 }, { hora: '09:00', cor: '#3EE47B', duration: 60 }, { hora: '10:15', cor: '#EB7A00', duration: 60 }, { hora: '11:15', cor: '#9D53B8', duration: 60 }] },
		// 	{ dia: 'Q', horarios: [{ hora: '08:00', cor: '#046AF0', duration: 60 }, { hora: '09:00', cor: '#3EE47B', duration: 60 }, { hora: '10:15', cor: '#EB7A00', duration: 60 }, { hora: '11:15', cor: '#9D53B8', duration: 60 }] },
		// 	{ dia: 'Q', horarios: [{ hora: '08:00', cor: '#046AF0', duration: 60 }, { hora: '09:00', cor: '#046AF0', duration: 60 }, { hora: '10:15', cor: '#EC135F', duration: 60 }, { hora: '11:15', cor: '#EC135F', duration: 60 }] },
		// 	{ dia: 'S', horarios: [{ hora: '08:00', cor: '#046AF0', duration: 60 }, { hora: '09:00', cor: '#3EE47B', duration: 60 }, { hora: '10:15', cor: '#EB7A00', duration: 60 }, { hora: '11:15', cor: '#9D53B8', duration: 60 }] },
		// 	{ dia: 'S', horarios: [{ hora: '08:00', cor: '#046AF0', duration: 60 }, { hora: '09:00', cor: '#3EE47B', duration: 60 }, { hora: '10:15', cor: '#EB7A00', duration: 60 }, { hora: '11:15', cor: '#9D53B8', duration: 60 }] }
		// ]
		// var legendBlock=info.legend;
		var legendBlock = [{ color: '#046AF0', title: 'Disciplina' }, { color: '#9D53B8', title: 'Lei Seca' }, { color: '#EC135F', title: 'Revisão' }, { color: '#EB7A00', title: 'Exercício' }, { color: '#3EE47B', title: 'Informativo' }];
		legendBlock.push({ color: '#DAD4C2', title: 'Intervalo' });
		//divisão de blocos de 15 em 15 minutos
		//o array de horas deve ser em ordem crescente e em formato string (hh:mm
		//checar a maior hora do dia
		var maxFragments = 0;
		var spaces = 0;
		let legend = [];
		data.map((current, index) => {
			var tempDuration = 0;
			current.horarios.map((subCurrent, key) => {
				if (legend.indexOf(subCurrent.hora) == -1) {
					legend.push({ time: subCurrent.hora, duration: subCurrent.duration });
				}
			});
		});
		legend = legend.sort((a, b) => { return moment(a.time, 'HH:mm').unix() - moment(b.time, 'HH:mm').unix(); });
		let temp = '';
		legend = legend.filter((current) => {
			if (temp == '') {
				temp = current.time;
				return true;
			} else {
				if (current.time != temp) {
					temp = current.time;
					return true;
				} else return false;
			}
		});
		var maxTime = parseInt(legend[legend.length - 1].time.split(':')[0]);
		var minMinutes = legend[0].time.split(':')[1];
		var minRowTime = parseInt(legend[0].time.split(':')[0]);
		var tempTime = moment(legend[legend.length - 1].time, 'HH:mm').subtract(moment(legend[0].time, 'HH:mm').hour(), 'H');
		tempTime.subtract(moment(legend[0].time, 'HH:mm').minute(), 'm');
		let fragments = (tempTime.hour() * 60) + tempTime.minute() + parseInt(legend[legend.length - 1].duration);
		maxFragments = fragments / div;
		var containerCol = [];
		var cols = [];
		var history = [];
		for (let x = 0; x < 7; x++) {
			let max = data[x].horarios.length;
			for (let key = 0; key < max; key++) {
				let current = data[x].horarios[key];
				let minTime = this.findMinTime(data, key);
				if (parseInt(current.hora.split(':')[0]) === minTime) {
					if (history.length && key === data[x].horarios.length - 1) {
						let response = this.historyProcess(history, minTime, current, cols, true);
						if (response) cols = this.makeFragment([style.blockStyle, { backgroundColor: response.cor }], this.checkBlankSpace(data, key - 1, x), cols, response.duration / div);
						else cols = this.makeFragment([style.blockStyle, { backgroundColor: '#DAD4C2' }], 0, cols, this.checkBlankSpace(data, key - 1, x));
					}
					if (key === data[x].horarios.length - 1 && key != 0 && parseInt(data[x].horarios[key - 1].hora.split(':')[0]) - parseInt(current.hora.split(':')[0]) > 1) {
						cols = this.makeFragment([style.blockStyle, { backgroundColor: '#DAD4C2' }], 0, cols, this.checkBlankSpace(data, key, x));
					}
					let params = key != 0 ? [data, key, x] : [data, key, x, minRowTime, minMinutes];
					cols = this.makeFragment([style.blockStyle, { backgroundColor: current.cor }], this.checkBlankSpace(...params), cols, current.duration / div);
				} else {
					if (history.length) {
						let response = this.historyProcess(history, minTime, current, cols);
						if (response) cols = this.makeFragment([style.blockStyle, { backgroundColor: response.cor }], this.checkBlankSpace(data, key - 1, x), cols, response.duration / div);
						else cols = this.makeFragment([style.blockStyle, { backgroundColor: '#DAD4C2' }], 0, cols, this.checkBlankSpace(data, key - 1, x));
					} else {
						history.push(current);
					}
				}
				if (max - 1 === key && history.length) {
					let indexRow = maxTime - minTime;
					for (let y = 1; y <= indexRow; y++) {
						if (history.length) {
							let rowTime = minTime + y;
							let response = this.historyProcess(history, rowTime, current, cols, true);
							if (response) {
								let params = key != 0 ? [data, key, x] : [data, key, x, minRowTime, minMinutes];
								cols = this.makeFragment([style.blockStyle, { backgroundColor: '#DAD4C2' }], 0, cols, this.checkBlankSpace(...params));
								cols = this.makeFragment([style.blockStyle, { backgroundColor: response.cor }], 0, cols, response.duration / div);
							}
						} else break;
					}
				}
				if (max - 1 === key && cols.length <= maxFragments) {
					for (let y = cols.length; y < maxFragments; y++) {
						cols.push(<Row key={key + y} style={[style.blockStyle, { backgroundColor: '#DAD4C2' }]}></Row>);
					}
				}
			};
			containerCol.push(
				<Col key={'col' + x}>
					{cols}
				</Col>
			);
			cols = [];
		}
		let initSpace = minMinutes / div;
		let colLegend = [];
		legend.map((current, key) => {
			let space = 0;
			if (legend[key + 1]) {
				let ini = moment(current.time, 'HH:mm');
				let end = moment(legend[key + 1].time, 'HH:mm');
				end.subtract(ini.hour(), 'H');
				end.subtract(ini.minute(), 'm');
				space = ((end.hour() * 60) + end.minute()) / div;
			} else space = 1;
			let pause = '';
			let last = '';
			//if (key!=0 && legend[key + 1] && moment(legend[key + 1].time, 'HH:mm').format('HH:mm') != moment(current.time, 'HH:mm').add(current.duration, 'm').format('HH:mm')) pause = moment(current.time, 'HH:mm').add(current.duration, 'm').format('H[H]mm');
			if (key === legend.length - 1) {
				pause = moment(current.time, 'HH:mm').add(current.duration, 'm').format('H[H]mm');
				space = (current.duration / div);
				last = true;
			}
			colLegend = this.makeFragment([style.blockStyle, { marginLeft: 10 }], 0, colLegend, space, { style: { position: 'absolute', fontSize: 9 }, content: moment(current.time, 'HH:mm').format('H[H]mm'), pause: pause, borderLine: true, last: last });
		});
		for (let y = colLegend.length; y < maxFragments; y++) {
			colLegend.push(<Row key={y} style={[style.blockStyle]}></Row>);
		}
		let legendWeek = [<Row key={0} style={[style.blockStyle]}></Row>];
		data.map((current, key) => {
			legendWeek = this.makeFragment([{ width: 40, height: 20, margin: 1, justifyContent: 'center' }], 0, legendWeek, 1, { style: { fontSize: 14, color: key == 0 || key == data.length - 1 ? '#C89FD7' : '#A15ABB', fontFamily: 'Montserrat-Regular' }, content: current.dia });
		});
		colLegend = <Col key={'colLegend'}>{colLegend}</Col>;
		containerCol.splice(0, 0, colLegend);
		let tempRow = [];
		let lgd = [];
		legendBlock.map((obj, key) => {
			if (key != 0 && key % 3 == 0) {
				lgd.push(<Row key={key}>{tempRow}</Row>);
				tempRow = [];
			}
			tempRow.push(<Col key={key} style={{ width: 100, flexDirection: 'row' }}><View style={{ backgroundColor: obj.color, width: 10, height: 10, margin: 4, borderRadius: 2 }} /><Text style={{ fontFamily: 'Montserrat-Light', fontSize: 12 }}>{obj.title}</Text></Col>);
		});
		if (tempRow.length) lgd.push(<Row key={moment().format('SSS')}>{tempRow}</Row>);
		return (
			<View style={{ backgroundColor: '#FFF' }}>
				<Row style={{ marginTop: 10 }}>
					{legendWeek}
				</Row>
				<Row>
					{containerCol}
				</Row>
				<View style={{ marginLeft: (width - 260) / 2, marginTop: 20, marginBottom: 20 }}>
					{lgd}
				</View>
			</View>);
	}

	historyProcess(history, minRowTime, current, cols, end) {
		let index = '';
		let his = history.filter((current, key) => {
			index = parseInt(current.hora.split(':')[0]) <= minRowTime && !index ? key + 1 : index;
			return parseInt(current.hora.split(':')[0]) <= minRowTime;
		})[0];
		if (his) {
			history.splice(index - 1, 1);
			if (!end) history.push(current);
			return his;
		} else {
			if (!end) history.push(current);
			return '';
		}
	}

	checkBlankSpace(data, index, day, minTime, minMinutes) {
		if (index != 0) {
			let dataIni = moment(data[day].horarios[index - 1].hora, 'HH:mm').add(data[day].horarios[index - 1].duration, 'm');
			let dataEnd = moment(data[day].horarios[index].hora, 'HH:mm');
			dataEnd.subtract(dataIni.hours(), 'H');
			dataEnd.subtract(dataIni.minutes(), 'm');
			let minute = dataEnd.hour() * 60;
			minute += dataEnd.minute();
			return minute / div;
		} else {
			if (minTime && minMinutes) {
				let dataIni = moment(minTime + ':' + minMinutes, 'HH:mm');
				let dataEnd = moment(data[day].horarios[index].hora, 'HH:mm');
				dataEnd.subtract(dataIni.hours(), 'H');
				dataEnd.subtract(dataIni.minutes(), 'm');
				let minute = dataEnd.hour() * 60;
				minute += dataEnd.minute();
				return minute / 15;
			} else return 0;
		}
	}

	makeFragment(stl, idx, cols, blocks, text) {
		let rows = [];
		for (let z = 0; z < idx; z++) {
			cols.push(<Row key={moment().format('SSS') + Math.random() + z} style={[style.blockStyle, { backgroundColor: '#DAD4C2' }, z == 0 && { borderTopLeftRadius: 5, borderTopRightRadius: 5 }, z == 0 && { marginTop: 4 }, z == idx - 1 && { borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }]}></Row>);
		}
		for (let y = 0; y < blocks; y++)cols.push(<Row key={moment().format('SSS') + Math.random() + y} style={stl.concat([{ borderTopWidth: 0 }, !text && y == 0 && { borderTopLeftRadius: 5, borderTopRightRadius: 5 }, !text && y == 0 && { marginTop: 4 }, text && y == 0 && { marginTop: 3 }, y == blocks - 1 && { borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }, y === 0 && text && text.borderLine && { borderTopWidth: 1, borderColor: '#000', paddingRight: 15 }, y == blocks - 1 && text && text.borderLine && text.pause ? text.last ? { borderBottomWidth: 1, borderColor: '#000', paddingRight: 15 } : { borderTopWidth: 1, borderColor: '#000', paddingRight: 15 } : ''])}>{y == 0 ? text ? <Text style={[text.style, { fontFamily: 'Montserrat-Light' }]}>{text.content}</Text> : undefined : y == blocks - 1 ? text ? text.pause ? <Text style={[text.style, { fontFamily: 'Montserrat-Light' }]}>{text.pause}</Text> : undefined : undefined : undefined}</Row>);
		return cols;
	}

	getMinHistoryTime(history) {
		let tempTime = 24;
		for (let idx in history) {
			let temp = parseInt(history[idx].content.hora.split(':')[0]);
			if (temp < tempTime) tempTime = temp;
		} return tempTime;
	}

	findMinTime(data, x) {
		let tempTime = 24;
		for (let idx in data) {
			let temp = data[idx].horarios[x] ? parseInt(data[idx].horarios[x].hora.split(':')[0]) : 24;
			if (temp < tempTime) tempTime = temp;
		}
		return tempTime;
	}

	test() {
		BridgeNative.executeAction('teste2', [], [], 'Controller');
	}

	render() {
		let inTime = false;
		let barSize = 180;
		let barColor = inTime ? '#3EE47B' : '#EC135F';
		let barIcon = inTime ? 'sucesso' : 'atencao';
		return (
			<Container>
				<Header style={style.header}>
					<Image style={style.img} source={require('../image/logo.png')} />
				</Header>
				<Content style={style.content}>
					{this.renderCalendar()}
					<View style={{ borderTopWidth: 1, borderColor: '#A9AEB1' }}>
						<Text>{this.props.store.dataProva}</Text>
						<Row style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 40, height: 50 }}>
							<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: -15, color: '#535D64' }}>13 AGO</Text>
							<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: (barSize - 20), color: '#535D64' }}>18 OUT</Text>
							<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: 240, color: '#122029' }}>30 OUT</Text>
							<Col style={{ width: 3, height: 15, backgroundColor: '#046AF0', borderRadius: 5, marginTop: -15 }} />
							<Col style={{ width: 3, height: 15, backgroundColor: '#046AF0', borderRadius: 5, marginTop: 10, position: 'absolute', marginLeft: barSize }} />
							<Col style={{ width: 250, height: 5, backgroundColor: barColor, marginTop: -15 }}>
								<Col style={{ width: barSize, height: 5, backgroundColor: '#046AF0' }} />
							</Col>
							<PlanMeFont size={22} color={barColor} style={{ marginTop: -13, marginLeft: -2 }} name={barIcon} />
							<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 35 : 35, fontSize: 10, marginLeft: -15, color: '#535D64' }}>INÍCIO</Text>
							<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 35 : 35, fontSize: 10, marginLeft: inTime ? (barSize - 8) : 253, color: inTime ? '#535D64' : '#122029' }}>FIM</Text>
							<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 35 : 35, fontSize: 10, marginLeft: !inTime ? (barSize - 13) : 245, color: !inTime ? '#535D64' : '#122029' }}>PROVA</Text>
						</Row>
					</View>
				</Content>
				<Footer style={{ backgroundColor: '#F2F3F3', height: 80, paddingTop: 10 }}>
					<Grid style={{ top: -40 }}>
						<Col>
							<Row style={{ marginLeft: 28, width: 150 }}>
								<View style={style.triangleLeft}></View>
								<Button style={[style.buttom, { left: -7 }]} textStyle={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }} onPress={Actions.adjustTime}>AJUSTAR</Button>
							</Row>
						</Col>
						<Col>
							<Row style={{ marginRight: 10, width: 150 }}>
								<Button style={[style.buttom, { left: 7 }]} textStyle={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }} onPress={Actions.progress}>CONCLUIR</Button>
								<View style={style.triangleRight}></View>
							</Row>
						</Col>
					</Grid>
				</Footer>
			</Container>);
	}
}
//Actions.adjustTime
const style = StyleSheet.create({
	header: {
		backgroundColor: '#fff',
		justifyContent: 'center',
	},
	img: {
		width: 80,
		height: 43
	},
	content: { backgroundColor: '#F2F3F3' },
	subtitle: {
		textAlign: 'left',
		flexWrap: 'wrap',
		lineHeight: 30,
		fontSize: 20,
		color: '#9D53B8',
		paddingLeft: 35,
		fontFamily: 'Montserrat-Regular',
		marginTop: 30
	},
	buttom: {
		flex: 1,
		backgroundColor: '#3EE47B',
		marginTop: 40,
		borderRadius: 5,
		height: 46,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	blockStyle: {
		width: 40,
		height: div * 0.75
	}
});