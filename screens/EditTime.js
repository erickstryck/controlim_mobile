import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Grid, Row, Col, Text, Button, View } from 'native-base';
import { SwipeRow } from 'react-native-swipe-list-view';
import { Actions } from 'react-native-mobx';
import PlanMeFont from '../Theme/PlanMeFont';
import CommomPage from './CommomPage';

export default class EditTime extends Component {

  constructor() {
    super();
    this.state = {
      options: [
        { name: 'Domingo', time: '9H ás 12 H' },
        { name: 'Segunda-Feira', time: '9H ás 12 H' },
        { name: 'Terça-Feira', time: '9H ás 12 H' },
        { name: 'Quarta-Feira', time: '9H ás 12 H' },
        { name: 'Quinta-Feira', time: '9H ás 12 H' },
        { name: 'Sexta-Feira', time: '9H ás 12 H' },
        { name: 'Sábado', time: '9H ás 12 H' },
      ]
    };
  }

  render() {
    let leftButton = <Button transparent style={{ width: 50 }} onPress={Actions.config}><PlanMeFont size={20} name='esquerda' color='#FFF' /></Button>

    let rows = this.state.options.map((o, k) => {
      return (
        <SwipeRow style={[styles.border]} disableRightSwipe={true} rightOpenValue={-120} key={k}>
          <View style={styles.standaloneRowBack}>
            <Text />
            <View style={[styles.backRightBtn, styles.backRightBtnLeft]}>
              <Button transparent style={styles.options}><PlanMeFont style={styles.icons} name='editar' /></Button>
            </View>
            <View style={[styles.backRightBtn, styles.backRightBtnRight]}>
              <Button transparent style={styles.options}><PlanMeFont style={styles.icons} name='excluir' /></Button>
            </View>
          </View>
          <View style={styles.standaloneRowFront}>
            <Text style={[styles.text, styles.border]}>{o.name}</Text>
            <Text style={[styles.text, styles.border, { fontFamily: 'Montserrat-Bold' }]}>{o.time}</Text>
          </View>
        </SwipeRow>
      );
    });

    return (
      <CommomPage title='TEMPO' page='config' leftButton={leftButton}>
        <Grid style={{ backgroundColor: '#F2F3F3' }}>
          <Row style={styles.white}>
            <Col>
              {rows}
            </Col>
          </Row>
        </Grid>
      </CommomPage>
    );
  }
};

const styles = StyleSheet.create({
  white: {
    backgroundColor: '#FFF', marginTop: 30
  },
  border: {
    borderBottomWidth: 1, borderBottomColor: '#ECEDEE'
  },
  standaloneRowFront: {
    padding: 20,
    flexDirection: 'row',
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 50,
  },
  standaloneRowBack: {
    alignItems: 'center',
    backgroundColor: '#F2F3F3',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 60
  },
  backRightBtnLeft: {
    backgroundColor: '#3EE47B',
    right: 60
  },
  backRightBtnRight: {
    backgroundColor: '#EC135F',
    right: 0
  },
  options: {
    marginLeft: 13,
    marginTop: 5
  },
  icons: {
    color: '#FFF',
    fontSize: 20
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    color: '#4D585F',
    fontSize: 14
  }
});