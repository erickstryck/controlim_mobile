import React, { Component } from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Container, Content, Text, Button } from 'native-base';
import PlanMeFont from '../Theme/PlanMeFont';
import { Actions } from 'react-native-mobx';
import { observer } from 'mobx-react/native';
import BridgeNative from '../controller/BridgeNative';

var { height, width } = Dimensions.get('window');
@observer
export default class Initial extends Component {

	constructor() {
		super();
		this.state = { action: '' };
		this.anonLogin = this.anonLogin.bind(this);
	}

	componentWillUpdate(nextProps, nextState) {
		if (!nextProps.store.error && nextProps.store.user) Actions.chooseChallenge();
	}

	anonLogin() {
		this.state.action = true;
		BridgeNative.executeAction('insert', [], [], 'Controller');
		//BridgeNative.executeAction('anonLogin',[],[], 'Auth');
	}

	render() {
		console.log(this.props.store.user);
		return (
			<Container>
				<Content>
					<Image style={style.img} source={require('../image/logo.png')} />
					<Row style={{ marginTop: 74 }}>
						<Button style={[style.buttom, { backgroundColor: '#4867AA' }]}>
							<PlanMeFont name='facebook' size={12} />
							<Text style={{ color: '#FFF', fontFamily: 'Montserrat-Bold' }}> FACEBOOK</Text>
						</Button>
					</Row>
					<Row style={{ marginTop: 20 }}>
						<Button onPress={Actions.login} style={style.buttom}><Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>LOGIN</Text></Button>
					</Row>
					<Row style={{ marginTop: 20 }}>
						<Button onPress={this.anonLogin} style={style.buttom}><Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>PULAR</Text></Button>
					</Row>
				</Content>
			</Container>
		);
	}
}

const style = StyleSheet.create({
	img: {
		marginLeft: (width / 2) - 100,
		marginTop: (height / 2) - 200
	},
	buttom: {
		backgroundColor: '#3EE47B',
		width: width - 50,
		marginLeft: 25,
		borderRadius: 5
	},
});