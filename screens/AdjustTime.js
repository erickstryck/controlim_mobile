import React, { Component } from 'react';
import { StyleSheet, Image, View, Dimensions, Text, Platform } from 'react-native';
import { Header, Container, Content, Footer, Button, Grid, Col, Row, Picker, CheckBox } from 'native-base';
import { Actions } from 'react-native-mobx';
import BridgeNative from '../controller/BridgeNative';

var { height, width } = Dimensions.get('window');
const Item = Picker.Item;

export default class AdjustTime extends Component {

  constructor() {
    super();
    this.state = {
      configuracoesTempo: '',
      all: '480'
    };
    this.state.names = {
      Do: 'Domingo',
      Se: 'Segunda-Feira',
      Te: 'Terça-Feira',
      Qa: 'Quarta-Feira',
      Qi: 'Quinta-Feira',
      Sx: 'Sexta-Feira',
      Sa: 'Sábado',
    }
    this.onValueChange = this.onValueChange.bind(this);
    this.onReady = this.onReady.bind(this);
  }

  componentWillMount() {
    this.state.configuracoesTempo = this.props.store.configuracoesTempo;
  }

  onValueChange(key, att, value) {
    let state = this.state;
    if (key === 'all' && att === 'all') {
      Object.keys(state.configuracoesTempo).map((key) => state.configuracoesTempo[key].minutosDiarios = value);
      state.all = value;
    }
    else {
      state.configuracoesTempo[key][att] = value;
    }
    this.setState(state);
  }

  onReady() {
    this.props.store.inputData('configuracoesTempo', this.state.configuracoesTempo);
    BridgeNative.executeAction('update', [{ path: 'users/' + this.props.store.user.uid + '/configuracoesTempo', obj: this.state.configuracoesTempo }], [], 'Controller');
    Actions.resumePlan();
  }

  render() {
    let hourNodes = [];
    hourNodes.push(<Item key={'90'} label={'1 HORA 30 MINUTOS'} value={90} />);
    for (let x = 2; x <= 15; x++) {
      hourNodes.push(<Item key={(x * 60)} label={x + ' HORAS'} value={x * 60} />);
    }

    let days = Object.keys(this.state.configuracoesTempo).map((key, idx) => {
      return (
        <Row key={idx} style={style.options}>
          <Col size={10}>
            <CheckBox checked={this.state.configuracoesTempo[key].checked} onPress={() => this.onValueChange(key, 'checked', !this.state.configuracoesTempo[key].checked)} />
          </Col>
          <Col size={50}>
            <Text style={style.text}>{this.state.names[key]}</Text>
          </Col>
          <Col size={40}>
            <Row style={style.rowPicker}>
              <Picker
                mode='dropdown'
                selectedValue={this.state.configuracoesTempo[key].minutosDiarios}
                onValueChange={(value) => this.onValueChange(key, 'minutosDiarios', value)}
                style={Platform.OS === 'android' ? [style.picker, { color: '#046AF0' }] : {}}>
                {hourNodes}
              </Picker>
            </Row>
          </Col>
        </Row>
      );
    });

    return (
      <Container>
        <Header style={style.header}>
          <Image style={style.img} source={require('../image/logo.png')} />
        </Header>
        <Content>
          <Row style={style.inputGr}>
            <Text style={style.subtitle}>Em quais dias você está{'\n'}disposto a estudar?</Text>
            <Row style={style.options}>
              <Col size={10} />
              <Col size={50}>
                <Text style={style.text}>Todos</Text>
              </Col>
              <Col size={40}>
                <Row style={style.rowPicker}>
                  <Picker
                    mode='dropdown'
                    selectedValue={this.state.all}
                    onValueChange={(value) => this.onValueChange('all', 'all', value)}
                    style={Platform.OS === 'android' ? [style.picker, { color: '#046AF0' }] : {}}>
                    {hourNodes}
                  </Picker>
                </Row>
              </Col>
            </Row>
            {days}
          </Row>
        </Content>
        <Footer style={{ backgroundColor: '#F2F3F3', height: 80, paddingTop: 10 }}>
          <Grid style={{ top: -35 }}>
            <Col>
              <Row style={{ marginLeft: ((width / 2) - 75), width: 150 }}>
                <Button style={[style.buttom, { left: 7 }]} textStyle={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }} onPress={this.onReady}>CONCLUIR</Button>
                <View style={style.triangleRight}></View>
              </Row>
            </Col>
          </Grid>
        </Footer>
      </Container>
    );
  }
}

const style = StyleSheet.create({
  header: {
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
  inputGr: {
    paddingLeft: 25,
    marginTop: 20,
    flexWrap: 'wrap',
    flexDirection: 'column',
  },
  subtitle: {
    textAlign: 'left',
    flexWrap: 'wrap',
    lineHeight: 30,
    fontSize: 20,
    color: '#9D53B8',
    paddingLeft: 15,
    fontFamily: 'Montserrat-Light'
  },
  rowPicker: {
    borderWidth: 1,
    borderColor: '#CCC',
    width: 120,
    borderRadius: 4,
    height: Platform.OS === 'ios' ? 40 : 50,
  },
  picker: {
    width: width - 50,
    justifyContent: 'flex-start',
  },
  img: {
    width: 80,
    height: 43
  },
  col: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    marginLeft: 3
  },
  buttom: {
    flex: 1,
    backgroundColor: '#3EE47B',
    marginTop: 40,
    borderRadius: 5,
    height: 46,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  options: {
    paddingBottom: 10,
    paddingTop: 10,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#EEE'
  }
});