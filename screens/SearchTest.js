import React, { Component } from 'react';
import { StyleSheet, Image, View, Dimensions, Text, Platform, TouchableWithoutFeedback, Modal } from 'react-native';
import { Header, Container, Content, Footer, Button, Grid, Col, Row, Picker, CheckBox, InputGroup, Input } from 'native-base';
import { Actions } from 'react-native-mobx';
import PlanMeFont from '../Theme/PlanMeFont';
import BridgeNative from '../controller/BridgeNative';
import Fetch from '../util/Fetch';
import ModalLoad from './ModalLoad';

var { height, width } = Dimensions.get('window');
const Item = Picker.Item;

export default class SearchTest extends Component {

  constructor() {
    super();
    this.state = {
      all: '',
      concursos: '',
      categorias: '',
      abrangencias: '',
      status: '',
      visible: false,
      filter: {
        categorias: [],
        abrangencias: [],
        status: []
      }
    };
    this.onSearch = this.onSearch.bind(this);
    this.onReady = this.onReady.bind(this);
    this.onFilter = this.onFilter.bind(this);
    this.modalVisible = this.modalVisible.bind(this);
  }

  componentWillMount() {
    Fetch.comunicate('https://us-central1-testefirebase-77a03.cloudfunctions.net/getConcursoCargo', 'POST', { uid: this.props.store.user.uid }, 'concursos');
  }

  componentWillUpdate(nextProps, nextState) {
    let concursos = this.props.store.concursos;

    let cargos = [];
    let categorias = [];
    let abrangencias = [];
    let status = [];
    concursos.map(concurso => {
      if (categorias.indexOf(concurso.concursoId.categoria.toLowerCase()) === -1) {
        categorias.push(concurso.concursoId.categoria.toLowerCase());
      }
      if (abrangencias.indexOf(concurso.concursoId.abrangencia.toLowerCase()) === -1) {
        abrangencias.push(concurso.concursoId.abrangencia.toLowerCase());
      }
      if (status.indexOf(concurso.concursoId.status.toLowerCase()) === -1) {
        status.push(concurso.concursoId.status.toLowerCase());
      }
      cargos.push({
        id: concurso.id,
        nome: concurso.concursoId.nome + ' - ' + concurso.nome,
        categoria: concurso.concursoId.categoria,
        status: concurso.concursoId.status,
        abrangencia: concurso.concursoId.abrangencia
      });
    });
    this.state.all = cargos;
    this.state.concursos = cargos;
    this.state.status = status.sort();
    this.state.categorias = categorias.sort();
    this.state.abrangencias = abrangencias.sort();
    this.props.store.closeModal();
  }

  onReady(val) {
    this.props.store.inputData('cargoSelecionado', val);
    Actions.chooseChallenge();
  }

  onSearch(val) {
    if (val === '') {
      this.setState({
        concursos: this.state.all
      });
    }
    else {
      let reg = new RegExp(val.toLowerCase(), 'g');
      this.setState({
        concursos: this.state.all.filter(c => c.nome.toLowerCase().match(reg))
      });
    }
  }

  onFilter(key, val) {
    let filter = this.state.filter;
    if (filter[key].indexOf(val) === -1) {
      filter[key].push(val);
    }
    else {
      filter[key].splice(filter[key].indexOf(val), 1);
    }

    let concursos = this.state.all;

    if (filter.categorias.length > 0) concursos = concursos.filter(c => filter.categorias.indexOf(c.categoria) !== -1);
    if (filter.abrangencias.length > 0) concursos = concursos.filter(c => filter.abrangencias.indexOf(c.abrangencia) !== -1);
    if (filter.status.length > 0) concursos = concursos.filter(c => filter.status.indexOf(c.status) !== -1);

    this.setState({
      concursos: concursos,
      filter: filter
    });
  }

  modalVisible() {
    this.setState({
      visible: !this.state.visible
    });
  }

  render() {
    let temp = this.props.store.concursos;
    let list = [];
    if (this.state.concursos.length) {
      list = this.state.concursos.map((concurso, k) => {
        return (
          <TouchableWithoutFeedback key={k} onPress={() => this.onReady(concurso)}>
            <Row style={style.option}>
              <Col size={90} style={style.colText}>
                <Text style={style.optionText}>{concurso.nome}</Text>
              </Col>
              <Col size={10} style={{ alignItems: 'center' }}>
                <PlanMeFont size={20} color='#9F57BA' name='direita' />
              </Col>
            </Row>
          </TouchableWithoutFeedback>
        );
      });
    }

    return (
      <Container>
        <Header style={style.header}>
          <Image style={style.img} source={require('../image/logo.png')} />
        </Header>
        <Content>
          <Row style={style.inputGr}>
            <Text style={style.subtitle}>Qual será o concurso?</Text>
          </Row>

          <Row style={{ marginTop: 20 }}>
            <Col size={90}>
              <InputGroup>
                <Input placeholder='Procurar' ref='search' onChangeText={this.onSearch} />
              </InputGroup>
            </Col>
            <Col size={10} style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Button transparent onPress={this.modalVisible}>
                <PlanMeFont name='filtro' size={20} color='#046AF0' />
              </Button>
            </Col>
          </Row>

          <Row style={style.list}>
            <Col>
              {list}
            </Col>
          </Row>
          {list.length ? <ModalFilter {...this.state} modalVisible={this.modalVisible} onFilter={this.onFilter} /> : null}
          <ModalLoad />
        </Content>
        <Footer style={{ backgroundColor: '#F2F3F3', height: 80, paddingTop: 10 }}>
          <Grid style={{ top: -35 }}>
            <Col>
              <Row style={{ marginLeft: ((width / 2) - 90), width: 180 }}>
                <Button
                  style={[style.buttom, { left: 7 }]}
                  textStyle={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}
                  onPress={() => this.onReady('keyNA')}>
                  NÃO ENCONTREI
                </Button>
                <View style={style.triangleRight}></View>
              </Row>
            </Col>
          </Grid>
        </Footer>
      </Container>
    );
  }
}

class ModalFilter extends Component {
  render() {
    let { visible, categorias, abrangencias, status } = this.props;

    let catNodes = categorias.map((c, k) => <FilterOption key={k} type='categorias' filter={this.props.filter} name={c} onFilter={this.props.onFilter} />);
    let abrNodes = abrangencias.map((c, k) => <FilterOption key={k} type='abrangencias' filter={this.props.filter} name={c} onFilter={this.props.onFilter} />);
    let stNodes = status.map((c, k) => <FilterOption key={k} type='status' filter={this.props.filter} name={c} onFilter={this.props.onFilter} />);

    return (
      <Modal animationType='slide' onRequestClose={() => { }} transparent={true} visible={visible}>
        <View style={style.modal}>
          <Container style={style.modalInner}>
            <Header style={[style.modalHearder, { backgroundColor: '#046AF0' }]}>
              <Row>
                <Col>
                  <Text style={style.mhText}>Filtro</Text>
                </Col>
              </Row>
            </Header>
            <Content style={{ width: width - 80, padding: 10 }}>
              {abrNodes}
              <Row style={style.hr} />
              {catNodes}
              <Row style={style.hr} />
              {stNodes}
            </Content>
            <Footer>
              <Row>
                <Col style={{ padding: 20 }}>
                  <Button style={style.buttomModal} onPress={this.props.modalVisible}><Text style={style.bText}>OK</Text></Button>
                </Col>
              </Row>
            </Footer>
          </Container>
        </View>
      </Modal>
    );
  }
}

class FilterOption extends Component {
  render() {
    return (
      <Row style={style.filterOption}>
        <Col size={10}>
          <CheckBox
            checked={this.props.filter[this.props.type].indexOf(this.props.name.toLowerCase()) !== -1}
            onPress={() => this.props.onFilter(this.props.type, this.props.name.toLowerCase())} />
        </Col>
        <Col size={90}>
          <Text style={style.text}>{this.props.name.replace(/(?:^|\s)\S/g, l => l.toUpperCase())}</Text>
        </Col>
      </Row>
    );
  }
}

const style = StyleSheet.create({
  header: {
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
  img: {
    width: 80,
    height: 43
  },
  inputGr: {
    paddingLeft: 25,
    marginTop: 20,
    flexWrap: 'wrap',
    flexDirection: 'column',
  },
  subtitle: {
    textAlign: 'left',
    flexWrap: 'wrap',
    lineHeight: 30,
    fontSize: 20,
    color: '#3EE47B',
    paddingLeft: 15,
    fontFamily: 'Montserrat-Light'
  },
  buttom: {
    flex: 1,
    backgroundColor: '#3EE47B',
    marginTop: 40,
    borderRadius: 5,
    height: 46,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  list: {
    marginTop: 20
  },
  option: {
    padding: 10,
    borderTopWidth: 1,
    borderTopColor: '#EEE'
  },
  colText: {
    justifyContent: 'center'
  },
  optionText: {
    fontFamily: 'Montserrat-Regular',
    color: '#9F57BA'
  },
  modal: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
    padding: 40,
  },
  modalInner: {
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  modalHearder: {
    height: 60,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  mhText: { padding: 10, color: '#FFF', fontWeight: 'bold' },
  filterOption: {
    marginTop: 5,
    alignItems: 'center'
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10
  },
  hr: {
    borderTopWidth: 1,
    height: 1,
    borderTopColor: '#CCC',
    marginTop: 20,
    marginBottom: 20
  },
  buttomModal: {
    backgroundColor: '#3EE47B',
    width: width - 120,
    marginTop: -12
  },
  bText: {
    fontSize: 17,
    fontFamily: 'Montserrat-Bold',
    color: '#046AF0',
    lineHeight: 17
  }
});