import React, { Component } from 'react';
import { Text, StyleSheet, Image, Dimensions, TextInput, DatePickerAndroid, DatePickerIOS, TouchableWithoutFeedback, TouchableOpacity, Platform, Modal } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Container, Content, Header, Title, Picker, Button, Footer, View, CheckBox } from 'native-base';
import PlanMeFont from '../Theme/PlanMeFont';
import { Actions } from 'react-native-mobx';
import BridgeNative from '../controller/BridgeNative';

var { height, width } = Dimensions.get('window');
const Item = Picker.Item;

const datePicker = {
	presetDate: new Date(2020, 4, 5),
	allDate: new Date(2020, 4, 5),
	simpleText: 'Selecione uma data',
	spinnerText: 'Selecione uma data',
	calendarText: 'Selecione uma data',
	defaultText: 'Selecione uma data',
	minText: 'Selecione uma data, não antes do dia de hoje'
}

export default class ChooseChallenge extends Component {
	constructor() {
		super();
		this.state = {
			selectedItem: undefined,
			hour: '480',
			dia: '',
			mes: '',
			ano: '',
			naoSei: false,
			date: new Date(),
			modal: false,
			timeZoneOffsetInHours: (-1) * (new Date()).getTimezoneOffset() / 60,
			inputGrBack1: '',
			inputGrBack2: '',
			inputGrBack3: '',
			inputGrPl2: 'DATA',
			inputGrPl3: '3 HORAS',
			inputGr1ColorHolder: '#046af0',
			inputGr2ColorHolder: '#046af0',
			inputGr3ColorHolder: '#046af0'
		}
		this.onValueChange = this.onValueChange.bind(this);
		this.datePicker = this.datePicker.bind(this);
		this.validate = this.validate.bind(this);
		this.onDateChangeIOS = this.onDateChangeIOS.bind(this);
	}

	onValueChange(value) {
		this.setState({
			hour: value
		});
	}

	async datePicker(stateKey, options) {
		try {
			var newState = {};
			const { action, year, month, day } = await DatePickerAndroid.open(options);
			if (action !== DatePickerAndroid.dismissedAction) {
				var date = new Date(year, month, day);
				this.state.dia = day < 9 ? '0' + day : day;
				this.state.mes = ++month < 9 ? '0' + month : month;
				this.state.ano = year;
			}
		} catch ({ code, message }) {
			console.warn(`Error in example '${stateKey}': `, message);
		}
		this.forceUpdate();
	}

	onDateChangeIOS(date) {
		let mes = date.getMonth();
		this.setState({
			date: date,
			dia: date.getDate() < 9 ? '0' + date.getDate() : date.getDate(),
			mes: ++mes < 9 ? '0' + mes : mes,
			ano: date.getFullYear(),
		});
	}

	validate() {
		var check = '';
		if (this.props.store.cargoSelecionado === 'ESCOLHA UMA PROVA') {
			this.props.store.inputData('cargoSelecionado', 'OBRIGATÓRIO!');
			this.state.inputGr1ColorHolder = '#ff0000';
		} else {
			check = true;
			this.state.inputGr1ColorHolder = '#046af0';
		}
		if (!this.state.naoSei && !this.state.dia) {
			this.state.inputGrPl2 = 'OBRIGATÓRIO!';
			this.state.inputGr2ColorHolder = '#ff0000';
			check = '';
		} else {
			check = true;
			this.state.inputGrPl2 = 'DATA';
			this.state.inputGr2ColorHolder = '#046af0';
		}
		if (check) {
			Object.keys(this.props.store.configuracoesTempo).map((c) => {
				this.props.store.configuracoesTempo[c].minutosDiarios = this.state.hour;
			});
			this.props.store.inputData('configuracoesTempo', this.props.store.configuracoesTempo);
			this.props.store.inputData('dataProva', this.state.dia + '/' + this.state.mes + '/' + this.state.ano);
			if (this.props.store.cargoSelecionado === 'keyNA') {
				Actions.chooseTest();
			} else {
				BridgeNative.executeAction('insertUser', [], [], 'Controller');
				Actions.resumePlan();
			}
		} else this.forceUpdate();
	}

	render() {
		let hourNodes = [];
		hourNodes.push(<Item key={'90'} label={'1 HORA 30 MINUTOS'} value={90} />);
		for (let x = 2; x <= 15; x++) {
			hourNodes.push(<Item key={(x * 60)} label={x + ' HORAS'} value={(x * 60)} />);
		}

		let text = '';
		let css = { marginTop: 7 };
		if (typeof this.props.store.cargoSelecionado === 'object') { text = this.props.store.cargoSelecionado.nome; css = {}; }
		else if (this.props.store.cargoSelecionado === 'keyNA') text = 'NÃO ENCONTREI'
		else text = this.props.store.cargoSelecionado;

		return (
			<Container>
				<Header style={style.header}>
					<Image style={style.img} source={require('../image/logo.png')} />
				</Header>
				<Content style={style.content}>
					<Grid>
						<Row style={style.inputGr}>
							<Text style={style.subtitle}>Qual é o nosso desafio?</Text>
							<Row style={[style.rowPicker, { borderColor: this.state.inputGr1ColorHolder, borderWidth: 1.5 }]}>
								<TouchableWithoutFeedback onPress={Actions.searchTest}>
									<Grid>
										<Col style={{ justifyContent: 'center' }}>
											<Text style={[css, style.picker, Platform.OS === 'android' && { color: this.state.inputGr1ColorHolder }]}>
												{text}
											</Text>
										</Col>
									</Grid>
								</TouchableWithoutFeedback>
							</Row>
						</Row>
						<Row style={style.inputGr}>
							<Text style={style.subtitle}>E quando será?</Text>
							<Row style={{ marginTop: 15, alignItems: 'center' }}>
								<CheckBox checked={this.state.naoSei} onPress={() => { this.setState({ naoSei: !this.state.naoSei }) }} />
								<Text style={[style.subtitle, { fontSize: 14, paddingRight: 25, color: '#FFF' }]}>Não sei</Text>
							</Row>
							{
								!this.state.naoSei ?
									<Row style={[style.rowPicker, { borderColor: this.state.inputGr2ColorHolder, borderWidth: 1.5 }]}>
										<TouchableWithoutFeedback onPress={Platform.OS === 'android' ? this.datePicker.bind(this, 'simple', { date: datePicker.simpleDate }) : () => { this.setState({ modal: true }) }}>
											<Grid>
												<Col><Text style={[style.dateSelectLeft, { color: Platform.OS === 'ios' ? '#000' : this.state.inputGr2ColorHolder }]}>{this.state.dia ? this.state.dia + '/' + this.state.mes + '/' + this.state.ano : this.state.inputGrPl2}</Text></Col>
												<Col style={style.dateSelectRight}><Text><PlanMeFont size={15} name='baixo' /></Text></Col>
											</Grid>
										</TouchableWithoutFeedback>
									</Row>
									:
									null
							}
						</Row>
						<Row style={style.inputGr}>
							<Text style={style.subtitle}>Quantas horas diárias serão{'\n'}dedicadas ao estudo?</Text>
							<Row style={style.rowPicker}>
								<Picker
									mode='dropdown'
									selectedValue={this.state.hour}
									onValueChange={this.onValueChange}
									style={Platform.OS === 'android' ? [style.picker, { color: '#046AF0' }] : {}}>
									{hourNodes}
								</Picker>
							</Row>
						</Row>
					</Grid>
					{
						Platform.OS === 'ios' ?
							<Modal animationType='slide' transparent={true} visible={this.state.modal}>
								<View style={style.modal}>
									<Header style={style.modalHeader}>
										<Row>
											<Col></Col>
										</Row>
										<Row>
											<Text style={style.mhText}>Selecione uma data</Text>
										</Row>
									</Header>
									<Row style={style.modalInner}>
										<Col>
											<Row>
												<Col>
													<DatePickerIOS
														date={this.state.date}
														mode='date'
														timeZoneOffsetInMinutes={this.state.timeZoneOffsetInHours * 60}
														onDateChange={this.onDateChangeIOS}
													/>
												</Col>
											</Row>
											<Row>
												<Col style={{ padding: 20 }}>
													<Button style={style.buttomModal} onPress={() => { this.setState({ modal: false }) }}><Text style={style.bText}>OK</Text></Button>
												</Col>
											</Row>
										</Col>
									</Row>
									<Row />
								</View>
							</Modal>
							:
							null
					}
				</Content>
				<Footer style={{ backgroundColor: '#046AF0', height: 80, paddingTop: 10, borderColor: '#046AF0' }}>
					<Grid style={{ top: -40 }}>
						<Col>
							<Row style={[style.arrow, { marginRight: 1.5 }]}>
								<View style={style.triangleLeft}></View>
								<TouchableOpacity style={[style.buttom, { backgroundColor: '#CCCFD1', left: -7 }]} onPress={Actions.initial}>
									<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>VOLTAR</Text>
								</TouchableOpacity>
							</Row>
						</Col>
						<Col>
							<Row style={[style.arrow, { marginLeft: 1.5 }]}>
								<TouchableOpacity style={[style.buttom, { left: 7 }]} onPress={this.validate}>
									<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>AVANÇAR</Text>
								</TouchableOpacity>
								<View style={style.triangleRight}></View>
							</Row>
						</Col>
					</Grid>
				</Footer>
			</Container>
		);
	}
}

const style = StyleSheet.create({
	header: {
		backgroundColor: '#fff',
		justifyContent: 'center'
	},
	img: {
		width: 80,
		height: 43
	},
	content: { backgroundColor: '#046af0' },
	title: {
		textAlign: 'left',
		flexWrap: 'wrap',
		lineHeight: 40,
		fontSize: 35,
		paddingTop: 30,
		padding: 15,
		paddingLeft: 35,
		fontFamily: 'Montserrat-Bold'
	},
	subtitle: {
		textAlign: 'left',
		flexWrap: 'wrap',
		lineHeight: 30,
		fontSize: 20,
		color: '#3EE47B',
		paddingLeft: 15,
		fontFamily: 'Montserrat-Light'
	},
	rowPicker: {
		backgroundColor: '#FFF',
		width: width - 50,
		marginTop: 20,
		borderRadius: 4,
		height: Platform.OS === 'ios' ? 40 : 50
	},
	picker: {
		width: width - 60,
		justifyContent: 'flex-start',
		marginLeft: 6,
		flex: 1,
		flexDirection: 'column',
		flexWrap: 'wrap'
	},
	inputGr: {
		paddingLeft: 25,
		marginTop: 20,
		flexWrap: 'wrap',
		flexDirection: 'column',
	},
	right: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	dateSelectRight: {
		padding: 11,
		paddingRight: 8,
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	dateSelectLeft: {
		fontSize: 17,
		marginTop: Platform.OS === 'ios' ? 9 : 12,
		paddingLeft: 8
	},
	timeSelectRight: {
		padding: 14,
		paddingRight: 8,
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	buttom: {
		flex: 1,
		backgroundColor: '#3EE47B',
		width: (width - 100) / 2,
		marginTop: 40,
		borderRadius: 5,
		height: 46,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	triangleLeft: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 22,
		borderRightWidth: 22,
		borderBottomWidth: 20,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#CCCFD1',
		transform: [{ rotate: '-90deg' }],
		top: 53,
		left: 6
	},
	triangleRight: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 22,
		borderRightWidth: 22,
		borderBottomWidth: 20,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#3EE47B',
		transform: [{ rotate: '90deg' }],
		top: 53,
		right: 6
	},
	arrow: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center'
	},
	modal: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		flex: 1,
		justifyContent: 'center',
		padding: 20,
	},
	modalInner: {
		borderRadius: 10,
		alignItems: 'center',
		backgroundColor: '#fff',
		borderTopLeftRadius: 0,
		borderTopRightRadius: 0,
		minHeight: 250
	},
	modalHeader: {
		height: 100,
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
		backgroundColor: '#046AF0'
	},
	mhText: { padding: 10, color: '#FFF', fontWeight: 'bold' },
	buttomModal: {
		backgroundColor: '#3EE47B',
		marginTop: 60,
		width: width - 80
	},
	bText: {
		fontSize: 17,
		fontFamily: 'Montserrat-Bold',
		color: '#046AF0',
		lineHeight: 17
	}
});