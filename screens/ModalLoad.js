import React, { Component } from 'react';
import Spinner from 'react-native-spinkit';
import Store from '../common/Store';
import { observer } from 'mobx-react/native';
import { StyleSheet, Modal, View } from 'react-native';
const store = Store.getInstance();

@observer
export default class ModalLoad extends Component {

  constructor() {
    super();
    this.state = {
      size: 100,
      color: '#FFFFFF'
    }
  }

  render() {
    return (
      <Modal animationType='slide' transparent={true} visible={store.modal} onRequestClose={() => { }}>
        <View style={styles.modal}>
          <Spinner style={styles.spinner} isVisible={true} size={this.state.size} type='9CubeGrid' color={this.state.color} />
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  spinner: {
    marginBottom: 50
  }
});