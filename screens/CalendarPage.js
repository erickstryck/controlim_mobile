import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Grid, Row, Col, Text, Button } from 'native-base';
import PlanMeFont from '../Theme/PlanMeFont';
import Calendar from 'react-native-calendar';
import moment from 'moment';
import ptMoment from 'moment/locale/pt-br';
moment.locale('pt-br', ptMoment);
import { Actions } from 'react-native-mobx';
import CommomPage from './CommomPage';

export default class CalendarPage extends Component {

  constructor() {
    super();
    this.state = {
      today: moment().format('YYYY-MM-DD'),
      todayEvents: [],
      events: [
        { date: '2017-01-14T09:00:00-02:00', title: 'Plan Me: Disciplina' },
        { date: '2017-01-14T10:00:00-02:00', title: 'Plan Me: Exercícios' },
        { date: '2017-01-15T08:00:00-02:00', title: 'Dentista' },
        { date: '2017-01-15T11:00:00-02:00', title: 'Plan Me: Revisão' },
        { date: '2017-01-18T13:00:00-02:00', title: 'Plan Me: Disciplina' },
        { date: '2017-01-18T14:00:00-02:00', title: 'Plan Me: Extras' },
        { date: '2017-01-18T16:00:00-02:00', title: 'Cinemais: Batman VS Superman' },
        { date: '2017-01-22T15:00:00-02:00', title: 'Plan Me: Disciplina' }
      ]
    };

    this.onDateSelect = this.onDateSelect.bind(this);
    this.handleToday = this.handleToday.bind(this);
  }

  componentWillMount() {
    this.onDateSelect(this.state.today);
  }

  onDateSelect(date) {
    this.setState({
      today: date,
      todayEvents: this.state.events.filter(e => moment(e.date).format('YYYY-MM-DD') === moment(date).format('YYYY-MM-DD'))
    });
  }

  handleToday(flag) {
    let today = flag === 'add' ? moment(this.state.today).add(1, 'day') : moment(this.state.today).subtract(1, 'day');
    this.setState({
      today: today
    });
  }

  render() {
    let todayEvents = [];
    if (this.state.todayEvents.length) {
      todayEvents = this.state.todayEvents.map((e, k) => {
        return (
          <Row key={k} style={styles.rowEvent}>
            <Col size={80}><Text style={styles.titleEvent}>{e.title}</Text></Col>
            <Col size={20}><Text style={styles.hourEvent}>{moment(e.date).format('HH:mm')}</Text></Col>
          </Row>
        );
      });
    }
    else {
      todayEvents = (
        <Row style={styles.rowEvent}>
          <Col size={80}><Text style={[styles.hourEvent, { textAlign: 'center' }]}>Nenhum evento marcado neste dia.</Text></Col>
        </Row>
      );
    }
    let rightButton = <Button transparent style={{ width: 50 }} onPress={Actions.addEvent}><PlanMeFont name='adicionar' style={{ color: '#FFF', fontSize: 23 }} /></Button>
    return (
      <CommomPage title='EVENTOS' page='calendar' rightButton={rightButton}>
        <Grid>
          <Row>
            <Col>
              <Calendar
                customStyle={calendar}
                selectedDate={this.state.today}
                eventDates={this.state.events.map(e => moment(e.date).format('YYYY-MM-DD'))}
                dayHeadings={['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb']}
                monthNames={['JANEIRO', 'FEVEREIRO', 'MARÇO', 'ABRIL', 'MAIO', 'JUNHO', 'JULHO', 'AGOSTO', 'SETEMBRO', 'OUTUBRO', 'NOVEMBRO', 'DEZEMBRO']}
                nextButtonText={<PlanMeFont name='direita' style={{ fontSize: 15, color: '#FFF' }} />}
                prevButtonText={<PlanMeFont name='esquerda' style={{ fontSize: 15, color: '#FFF' }} />}
                showControls={true}
                scrollEnabled={true}
                showEventIndicators={true}
                weekStart={0}
                onDateSelect={(date) => this.onDateSelect(date)}
              />
            </Col>
          </Row>

          <Row>
            <Col style={styles.today}>
              <Button transparent style={styles.btDate} onPress={() => this.handleToday('subtract')}>
                <PlanMeFont name='esquerda' style={{ fontSize: 15, color: '#FFF' }} />
              </Button>
              <Text style={styles.todayTitle}>{moment(this.state.today).format('ddd - DD / MMM').toUpperCase()}</Text>
              <Button transparent style={[styles.btDate, { justifyContent: 'flex-end' }]} onPress={() => this.handleToday('add')}>
                <PlanMeFont name='direita' style={{ fontSize: 15, color: '#FFF' }} />
              </Button>
            </Col>
          </Row>

          <Row>
            <Col>
              {todayEvents}
            </Col>
          </Row>
        </Grid>
      </CommomPage>
    );
  }
}

const calendar = {
  title: {
    color: '#FFF',
    fontSize: 16,
    fontFamily: 'Montserrat-Bold'
  },
  calendarContainer: {
    backgroundColor: '#FFF'
  },
  calendarControls: {
    backgroundColor: '#3EE47B',
    borderRadius: 10,
    margin: 10
  },
  calendarHeading: {
    borderWidth: 0,
    borderColor: '#FFF'
  },
  currentDayCircle: {
    backgroundColor: '#9D53B8',
    borderRadius: 0,
    padding: 0
  },
  currentDayText: {
    color: '#046AF0',
    fontWeight: 'bold'
  },
  selectedDayCircle: {
    backgroundColor: '#9D53B8',
    borderRadius: 0,
    padding: 0
  },
  day: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: '#545E65'
  },
  dayButton: {
    padding: 0
  },
  dayHeading: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: '#545E65'
  },
  dayCircleFiller: {
    padding: 0,
    width: 53,
    height: 35
  },
  hasEventText: {
    fontWeight: 'bold'
  },
  eventIndicator: {
    width: 53,
    height: 2,
    marginTop: -1,
    marginBottom: -1,
    backgroundColor: '#72ABF7'
  },
  eventIndicatorFiller: {
    margin: 0,
    padding: 0,
    width: 0,
    height: 0,
  },
  weekendHeading: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: '#545E65'
  },
  weekendDayText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: '#545E65'
  }
};

const styles = StyleSheet.create({
  today: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#9D53B8',
    borderRadius: 10,
    margin: 10
  },
  todayTitle: {
    fontFamily: 'Montserrat-Bold',
    color: '#FFF',
    fontSize: 14
  },
  rowEvent: {
    padding: 10,
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#E1E2E4',
    marginLeft: 10,
    marginRight: 10
  },
  titleEvent: {
    fontFamily: 'Montserrat-Regular',
    color: '#59636A',
    fontSize: 14,
  },
  hourEvent: {
    color: '#59636A',
    fontSize: 14,
    fontFamily: 'Montserrat-Bold',
    textAlign: 'right'
  },
  btDate: {
    width: 50,
    height: 45,
    justifyContent: 'flex-start'
  }
});