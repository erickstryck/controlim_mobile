import React, { Component } from 'react';
import { StyleSheet, Dimensions, Modal } from 'react-native';
import { Container, Header, Content, Grid, Row, Col, Text, Button, View } from 'native-base';
import PlanMeFont from '../Theme/PlanMeFont';
var { height, width } = Dimensions.get('window');

import CommomPage from './CommomPage';

export default class ActivitiePage extends Component {
  render() {
    return (
      <CommomPage title='ATIVIDADE' page='activities'>
        <Grid style={{ backgroundColor: '#F2F3F3' }}>
          <Row style={{ backgroundColor: '#FFF' }}>
            <Col style={[styles.options, styles.border]}>
              <Row><PlanMeFont style={styles.oIcon} name='fiz' /></Row>
              <Row><Button style={styles.oTitle}>FIZ</Button></Row>
            </Col>
            <Col style={[styles.options, styles.border]}>
              <Row><PlanMeFont style={styles.oIcon} name='naoFiz' /></Row>
              <Row><Button style={styles.oTitle}>NÃO FIZ</Button></Row>
            </Col>
          </Row>

          <Row style={{ backgroundColor: '#FFF', marginTop: 30 }}>
            <Col style={[styles.options, styles.border]} size={35}>
              <Row><PlanMeFont style={styles.oIcon} name='disciplina' /></Row>
              <Row><Button style={[styles.oTitle, { width: 80 }]}>EDITAR</Button></Row>
            </Col>
            <Col style={[styles.options, styles.border, { alignItems: 'flex-start' }]} size={65}>
              <Text style={styles.eDiscipline}>Direito Civil (Parte Geral), v.1</Text>
              <Text style={styles.eAuthor}>VENOSA, Silvio de Salvo</Text>
            </Col>
          </Row>

          <Row style={[styles.border, { backgroundColor: '#FFF', marginTop: 30 }]}>
            <Col style={styles.time} size={20}>
              <PlanMeFont style={styles.tIcon} size={25} name='tempo' />
            </Col>
            <Col size={80}>
              <Row style={styles.border}>
                <Col size={80}><Text style={styles.tText}>Ínicio</Text></Col>
                <Col size={20}><Text style={styles.tTime}>9H</Text></Col>
              </Row>
              <Row>
                <Col size={80}><Text style={styles.tText}>Fim</Text></Col>
                <Col size={20}><Text style={styles.tTime}>10H</Text></Col>
              </Row>
            </Col>
          </Row>

          <Row style={[styles.border, { backgroundColor: '#FFF', marginTop: 30 }]}>
            <Col style={styles.time} size={20}>
              <PlanMeFont style={styles.tIcon} size={25} name='marcador' />
            </Col>
            <Col size={80}>
              <Row style={styles.border}>
                <Col size={80}><Text style={styles.tText}>Páginas</Text></Col>
                <Col size={20}><Text style={styles.tTime}>30</Text></Col>
              </Row>
              <Row>
                <Col><Text style={styles.tText}>Adicionar Intervalo</Text></Col>
              </Row>
            </Col>
          </Row>

          <Row style={{ backgroundColor: '#FFF', marginTop: 30 }}>
            <Col style={[styles.border, styles.emoticons]}><PlanMeFont name='triste' size={40} color='#848B90' /></Col>
            <Col style={[styles.border, styles.emoticons]}><PlanMeFont name='infeliz' size={40} color='#848B90' /></Col>
            <Col style={[styles.border, styles.emoticons]}><PlanMeFont name='neutro' size={40} color='#848B90' /></Col>
            <Col style={[styles.border, styles.emoticons]}><PlanMeFont name='feliz' size={40} color='#848B90' /></Col>
          </Row>
        </Grid>
        <ModalActivitie visible={false} done={true} />
      </CommomPage>
    );
  }
}

class ModalActivitie extends Component {
  render() {
    let { visible, done } = this.props;
    let color = done ? '#046AF0' : '#EC135F';
    return (
      <Modal animationType='slide' transparent={true} visible={visible}>
        <View style={styles.modal}>
          <Container style={styles.modalInner}>
            <Header style={[styles.modalHearder, { backgroundColor: color }]}>
              <Row style={{ flexDirection: 'column' }}>
                <Col style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <View style={[styles.step, { backgroundColor: '#3EE47B' }]} />
                  <View style={styles.step} />
                  <View style={styles.step} />
                </Col>
                <Col style={{ marginTop: -40 }}>
                  <Text style={styles.mhText}>Na bibliogradia indicada, você estudou quais páginas?</Text>
                </Col>
              </Row>
            </Header>
          </Container>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  border: {
    borderBottomWidth: 1,
    borderBottomColor: '#A1A7AA'
  },
  options: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 25
  },
  oIcon: {
    fontSize: 50,
    color: '#7F868C'
  },
  oTitle: {
    backgroundColor: '#9D53B8',
    width: 130,
    height: 25
  },
  eDiscipline: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#5D676E'
  },
  eAuthor: {
    color: '#798186',
    marginTop: 20,
    fontSize: 14
  },
  time: {
    alignItems: 'center'
  },
  tIcon: {
    color: '#606970',
    marginTop: 10
  },
  tText: {
    color: '#1D79F2',
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10
  },
  tTime: {
    textAlign: 'right',
    color: '#AEB3B6',
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 10,
    marginRight: 20
  },
  emoticons: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100
  },
  modal: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
    padding: 40,
  },
  modalInner: {
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  modalHearder: {
    height: 100,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  mhText: { padding: 10, color: '#FFF', fontWeight: 'bold' },
  step: {
    borderRadius: 50,
    width: 8,
    height: 8,
    backgroundColor: '#FFF',
    marginRight: 3,
    marginLeft: 3
  }
});