import React, { Component } from 'react';
import { StyleSheet, Dimensions, Platform } from 'react-native';
import { Content, Button, Grid, Row, Col, Text } from 'native-base';
import PlanMeFont from '../Theme/PlanMeFont';

import CommomPage from './CommomPage';
var { height, width } = Dimensions.get('window');

export default class ProgressPage extends Component {

	constructor() {
		super();
		this.state = {
			humor: {
				excited: 66,
				happy: 29,
				neutral: 45,
				sad: 8
			}
		};
	}

	render() {
		let inTime = true;
		let barSize = 180;
		let barColor = inTime ? '#3EE47B' : '#EC135F';
		let barIcon = inTime ? 'sucesso' : 'atencao';

		let max = this.state.humor.excited;
		max = this.state.humor.happy > max ? this.state.humor.happy : max
		max = this.state.humor.neutral > max ? this.state.humor.neutral : max
		max = this.state.humor.sad > max ? this.state.humor.sad : max
		max = max === 0 ? 1 : max;
		let excited = ((((this.state.humor.excited * 100) / max) * 200) / 100);
		let happy = ((((this.state.humor.happy * 100) / max) * 200) / 100);
		let neutral = ((((this.state.humor.neutral * 100) / max) * 200) / 100);
		let sad = ((((this.state.humor.sad * 100) / max) * 200) / 100);

		excited = excited < 10 ? 10 : excited;
		happy = happy < 10 ? 10 : happy;
		neutral = neutral < 10 ? 10 : neutral;
		sad = sad < 10 ? 10 : sad;

		return (
			<CommomPage title='PROGRESSO' page='progress'>
				<Grid style={{ paddingBottom: 20 }}>
					<Content style={styles.progress}>
						<Row style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 40, height: 50 }}>
							<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: -15, color: '#535D64' }}>02 FEV</Text>
							<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: (barSize - 20), color: '#535D64' }}>20 MAR</Text>
							<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: 240, color: '#122029' }}>02 ABR</Text>
							<Col style={{ width: 3, height: 15, backgroundColor: '#8C9397', borderRadius: 5, marginTop: -15 }} />
							<Col style={{ width: 3, height: 15, backgroundColor: '#8C9397', borderRadius: 5, marginTop: 10, position: 'absolute', marginLeft: barSize }} />
							<Col style={{ width: 250, height: 5, backgroundColor: barColor, marginTop: -15 }}>
								<Col style={{ width: barSize, height: 5, backgroundColor: '#8C9397' }} />
							</Col>
							<PlanMeFont size={22} color={barColor} style={{ marginTop: -13, marginLeft: -2 }} name={barIcon} />
							<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 25 : 20, fontSize: 10, marginLeft: -15, color: '#535D64' }}>INÍCIO</Text>
							<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 25 : 20, fontSize: 10, marginLeft: inTime ? (barSize - 8) : 271, color: inTime ? '#535D64' : '#122029' }}>FIM</Text>
							<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 25 : 20, fontSize: 10, marginLeft: !inTime ? (barSize - 8) : 245, color: !inTime ? '#535D64' : '#122029' }}>PROVA</Text>
						</Row>
					</Content>

					<Row style={styles.hours}>
						<Row>
							<Col style={styles.hText}><Text style={{ fontFamily: 'Montserrat-Regular' }}>Horas dedicadas em cada atividade:</Text></Col>
						</Row>
						<Row>
							<Col style={styles.hIcons}>
								<PlanMeFont color='#5E686E' size={50} name='disciplina' />
								<Text style={styles.hTitle}>DISCIPLINAS</Text>
								<Text style={styles.hValues}>200H</Text>
							</Col>
							<Col style={styles.hIcons}>
								<PlanMeFont color='#5E686E' size={50} name='exercicio' />
								<Text style={styles.hTitle}>EXERCÍCIO</Text>
								<Text style={styles.hValues}>134H</Text>
							</Col>
							<Col style={styles.hIcons}>
								<PlanMeFont color='#5E686E' size={50} name='lei' />
								<Text style={styles.hTitle}>LEI SECA</Text>
								<Text style={styles.hValues}>11H</Text>
							</Col>
						</Row>
						<Row style={{ marginTop: 20, marginBottom: 20 }}>
							<Col style={styles.hIcons}>
								<PlanMeFont color='#5E686E' size={50} name='informativo' />
								<Text style={styles.hTitle}>INFORMATIVO</Text>
								<Text style={styles.hValues}>51H</Text>
							</Col>
							<Col style={styles.hIcons}>
								<PlanMeFont color='#5E686E' size={50} name='revisao' />
								<Text style={styles.hTitle}>REVISÃO</Text>
								<Text style={styles.hValues}>8H</Text>
							</Col>
							<Col style={styles.hIcons}>
								<PlanMeFont color='#5E686E' size={50} name='extra' />
								<Text style={styles.hTitle}>EXTRA</Text>
								<Text style={styles.hValues}>32H</Text>
							</Col>
						</Row>
					</Row>

					<Row style={styles.humor}>
						<Row>
							<Col style={styles.hText}><Text style={{ fontFamily: 'Montserrat-Regular' }}>Até agora, como foi meu humor:</Text></Col>
						</Row>
						<Row style={styles.line}>
							<Col size={20} style={styles.barInfo}>
								<Text style={styles.barText}>{this.state.humor.excited}</Text>
							</Col>
							<Col size={80} style={{ flexDirection: 'row' }}>
								<Col style={{ width: excited, height: 12, backgroundColor: '#CCCFD1', borderRadius: 5, marginTop: 20 }} />
								<PlanMeFont style={{ marginLeft: -8 }} name='feliz' size={40} color='#3EE47B' />
							</Col>
						</Row>

						<Row style={styles.line}>
							<Col size={20} style={styles.barInfo}>
								<Text style={styles.barText}>{this.state.humor.happy}</Text>
							</Col>
							<Col size={80} style={{ flexDirection: 'row' }}>
								<Col style={{ width: happy, height: 12, backgroundColor: '#CCCFD1', borderRadius: 5, marginTop: 20 }} />
								<PlanMeFont style={{ marginLeft: -8 }} name='neutro' size={40} color='#046AF0' />
							</Col>
						</Row>

						<Row style={styles.line}>
							<Col size={20} style={styles.barInfo}>
								<Text style={styles.barText}>{this.state.humor.neutral}</Text>
							</Col>
							<Col size={80} style={{ flexDirection: 'row' }}>
								<Col style={{ width: neutral, height: 12, backgroundColor: '#CCCFD1', borderRadius: 5, marginTop: 20 }} />
								<PlanMeFont style={{ marginLeft: -8 }} name='infeliz' size={40} color='#9D53B8' />
							</Col>
						</Row>

						<Row style={styles.line}>
							<Col size={20} style={styles.barInfo}>
								<Text style={styles.barText}>{this.state.humor.sad}</Text>
							</Col>
							<Col size={80} style={{ flexDirection: 'row' }}>
								<Col style={{ width: sad, height: 12, backgroundColor: '#CCCFD1', borderRadius: 5, marginTop: 20 }} />
								<PlanMeFont style={{ marginLeft: -8 }} name='triste' size={40} color='#EC135F' />
							</Col>
						</Row>
					</Row>

					<Row style={styles.exercicies}>
						<Row>
							<Col style={styles.hText}><Text style={{ fontFamily: 'Montserrat-Regular' }}>Resultado dos Exercícios:</Text></Col>
						</Row>
						<Row>
							<Col style={styles.results}><Text style={[styles.texts, styles.errors]}>32</Text><Text style={styles.hint}>erros</Text></Col>
							<Col style={styles.results}><Text style={[styles.texts, styles.successes]}>179</Text><Text style={styles.hint}>acertos</Text></Col>
						</Row>
					</Row>

					<Row style={styles.options}>
						<Row style={styles.oItem}>
							<Button style={styles.buttom}><Text style={styles.bText}>PROGRESSO LEI SECA</Text></Button>
						</Row>
						<Row style={styles.oItem}>
							<Button style={styles.buttom}><Text style={styles.bText}>PROGRESSO DO EDITAL</Text></Button>
						</Row>
						<Row style={styles.oItem}>
							<Button style={styles.buttom}><Text style={styles.bText}>MINHAS PENDÊNCIAS</Text></Button>
						</Row>
						<Row style={styles.oItem}>
							<Button style={styles.buttom}><Text style={styles.bText}>MINHAS ANOTAÇÕES</Text></Button>
						</Row>
					</Row>
				</Grid>
			</CommomPage>
		);
	}
}

const styles = StyleSheet.create({
	progress: {
		height: 100,
		borderBottomColor: '#9AA0A4',
		borderBottomWidth: 1
	},
	hours: {
		backgroundColor: '#F2F3F3',
		flexDirection: 'column'
	},
	hText: {
		marginTop: 20,
		marginBottom: 20,
		marginLeft: 30,
		flex: 1,
		flexDirection: 'row'
	},
	hIcons: {
		flex: 1,
		padding: 10,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	hTitle: {
		fontFamily: 'Montserrat-Regular',
		backgroundColor: '#3EE47B',
		color: '#FFF',
		fontSize: 10,
		lineHeight: 10,
		width: 90,
		textAlign: 'center',
		marginTop: 5,
		marginBottom: 5,
		paddingTop: 5,
		paddingBottom: 5
	},
	hValues: {
		fontFamily: 'Montserrat-UltraLight',
		fontSize: 18,
		color: '#525C63',
		fontWeight: '300'
	},
	humor: {
		flexDirection: 'column',
		borderBottomColor: '#9AA0A4',
		borderBottomWidth: 1
	},
	exercicies: {
		backgroundColor: '#F2F3F3',
		flexDirection: 'column'
	},
	results: {
		flexDirection: 'row',
		alignItems: 'flex-end',
		justifyContent: 'center',
		marginBottom: 20
	},
	texts: {
		fontFamily: 'Montserrat-UltraLight',
		fontSize: 40,
		lineHeight: 40,
		fontWeight: '300'
	},
	errors: {
		color: '#EC1D66',
	},
	successes: {
		color: '#5CE68F',
	},
	hint: {
		fontFamily: 'Montserrat-Regular',
		marginLeft: 10,
		color: '#9CA2A5'
	},
	options: {
		flexDirection: 'column'
	},
	oItem: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	buttom: {
		backgroundColor: '#3EE47B',
		width: width - 50,
		marginTop: 25,
		borderRadius: 5
	},
	bText: {
		fontFamily: 'Montserrat-Bold',
		color: '#046AF0',
		lineHeight: 12
	},
	line: {
		justifyContent: 'center',
		alignItems: 'center'
	},
	barInfo: {
		justifyContent: 'center',
		alignItems: 'center'
	},
	barText: {
		fontFamily: 'Montserrat-Regular',
		fontSize: 20,
		color: '#4D585F'
	}
});

if (Platform.OS === 'ios') {
	styles.bText.fontWeight = 'bold'
}