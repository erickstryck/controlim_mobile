import React, { Component } from 'react';
import { Text, StyleSheet, Image, Dimensions, TouchableOpacity, View, Alert } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Header, Title, Input, Container, Content, Footer, ListItem, List, CheckBox } from 'native-base';
import { Actions } from 'react-native-mobx';
import BridgeNative from '../controller/BridgeNative';
import ModalLoad from './ModalLoad';

var { height, width } = Dimensions.get('window');

export default class ChooseTestP2 extends Component {
	constructor() {
		super();
		this.validate = this.validate.bind(this);
		this.alertUi = this.alertUi.bind(this);
		this.check = this.check.bind(this);
	}

	componentWillMount() {
		// BridgeNative.executeAction('inserirDisciplina',[],[], 'Controller');
		BridgeNative.executeAction('getOnce', [{ path: 'disciplinas' }], [], 'Controller');
	}

	componentWillUpdate(nextProps, nextState) {
		if (nextProps.store.disciplinasSelecionadas) this.props.store.closeModal(5000);
	}

	validate() {
		let index = this.refs.list.props.children.length;
		let continuar = '';
		for (let x = 0; x < index; x++) {
			if (this.refs['select_' + x].refs.check.props.checked) {
				continuar = true;
				break;
			} else {
				continuar = '';
			}
		}
		if (continuar) {
			Actions.chooseTest3();
		} else {
			this.alertUi();
		}
	}

	alertUi() {
		Alert.alert(
			'Ooops!',
			'Selecione pelo menos uma disciplina para continuar.',
			[{ text: 'Entendi' }],
			{ cancelable: false }
		)
	}

	check(d, checked) {
		let temp = this.props.store.disciplinasSelecionadas;
		if (checked) {
			temp.push(d);
		}
		else {
			temp = temp.filter((_d) => _d._id !== d._id);
		}
		this.props.store.inputData('disciplinasSelecionadas', temp);
	}

	render() {

		let listNode = Object.keys(this.props.store.disciplinas).map((d, x) => <ItemChallenge ref={'select_' + x} key={x} chave={d} nome={this.props.store.disciplinas[d]} check={this.check} />);

		return (
			<Container>
				<Header style={style.header}>
					<Image style={style.img} source={require('../image/logo.png')} />
				</Header>
				<Content style={style.content}>
					<Text style={style.subtitle}>Quais disciplinas estão{'\n'}previstas no edital?</Text>
					<List ref='list'>
						{listNode}
					</List>
					<ModalLoad />
				</Content>
				<Footer style={{ backgroundColor: 'transparent', height: 80, paddingTop: 10 }}>
					<Grid style={{ top: -40 }}>
						<Col>
							<Row style={[style.arrow, { marginRight: 1.5 }]}>
								<View style={style.triangleLeft}></View>
								<TouchableOpacity style={[style.buttom, { backgroundColor: '#CCCFD1', left: -7 }]} onPress={Actions.chooseTest}>
									<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>VOLTAR</Text>
								</TouchableOpacity>
							</Row>
						</Col>
						<Col>
							<Row style={[style.arrow, { marginLeft: 1.5 }]}>
								<TouchableOpacity style={[style.buttom, { left: 7 }]} onPress={this.validate}>
									<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>AVANÇAR</Text>
								</TouchableOpacity>
								<View style={style.triangleRight}></View>
							</Row>
						</Col>
					</Grid>
				</Footer>
			</Container>
		);
	}
}

class ItemChallenge extends Component {
	constructor() {
		super();
		this.state = {
			check: false
		};
		this.handleCheck = this.handleCheck.bind(this);
	}

	handleCheck() {
		let temp = {};
		temp[this.props.chave] = this.props.nome;
		this.props.check(temp, !this.state.check);
		this.setState({ check: !this.state.check });
	}

	render() {
		return (
			<ListItem onPress={this.handleCheck}>
				<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Regular' }}>{this.props.nome}</Text>
				<CheckBox onPress={this.handleCheck} ref='check' checked={this.state.check} />
			</ListItem>
		);
	}
}

const style = StyleSheet.create({
	header: {
		backgroundColor: '#fff',
		justifyContent: 'center'
	},
	img: {
		width: 80,
		height: 43
	},
	content: { backgroundColor: '#fff' },
	subtitle: {
		textAlign: 'left',
		flexWrap: 'wrap',
		lineHeight: 30,
		fontSize: 20,
		color: '#9D53B8',
		paddingLeft: 35,
		fontFamily: 'Montserrat-Regular',
		marginTop: 30
	},
	buttom: {
		flex: 1,
		backgroundColor: '#3EE47B',
		marginTop: 40,
		borderRadius: 5,
		height: 46,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	triangleLeft: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 22,
		borderRightWidth: 22,
		borderBottomWidth: 20,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#CCCFD1',
		transform: [{ rotate: '-90deg' }],
		top: 53,
		left: 6
	},
	triangleRight: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 22,
		borderRightWidth: 22,
		borderBottomWidth: 20,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#3EE47B',
		transform: [{ rotate: '90deg' }],
		top: 53,
		right: 6
	},
	arrow: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center'
	}
});