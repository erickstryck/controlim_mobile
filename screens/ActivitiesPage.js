import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';
import { Content, Grid, Row, Col, Text, Button } from 'native-base';
import PlanMeFont from '../Theme/PlanMeFont';
import { Actions } from 'react-native-mobx';
import CommomPage from './CommomPage';

export default class ActivitiesPage extends Component {

  constructor() {
    super();
    this.state = {
      activities: [
        { category: 'D', do: 30, done: 22, hour: '09H', status: 'done', name: 'Direito Civil' },
        { category: 'E', do: 50, done: 23, hour: '10H', status: 'open', name: 'Direito Civil' },
        { category: 'L', do: 10, done: 0, hour: '11H', status: 'not', name: 'Direito Penal' },
        { category: 'I', do: 20, done: 0, hour: '12H', status: 'open', name: 'Novas Legislações' },
        { category: 'R', do: 30, done: 0, hour: '14H', status: 'open', name: 'Direito Empresarial' },
        { category: 'X', do: 30, done: 0, hour: '15H', status: 'open', name: 'Direito Civil' },
      ]
    };
  }

  render() {

    let results = this.state.activities.map((a, k) => {
      let doText, category, icon;
      switch (a.category) {
        case 'D': doText = ' páginas'; category = 'DISCIPLINA'; icon = 'disciplina'; break;
        case 'E': doText = ' exercícios'; category = 'EXERCÍCIO'; icon = 'exercicio'; break;
        case 'L': doText = ' artigos'; category = 'LEI SECA'; icon = 'lei'; break;
        case 'I': doText = ' páginas'; category = 'INFORMATIVO'; icon = 'informativo'; break;
        case 'R': doText = ' páginas'; category = 'REVISÃO'; icon = 'revisao'; break;
        case 'X': doText = ' páginas'; category = 'EXTRA'; icon = 'extra'; break;
      }
      doText = a.status === 'open' ? a.do + doText : a.status === 'done' ? a.done + '/' + a.do : a.do + doText;
      let bar = a.status === 'done' ? (((a.done * 100) / a.do) * 190) / 100 : 0;

      return (
        <Row key={k}>
          <Col>
            <Row>
              <Text style={styles.hour}>{a.hour}</Text>
            </Row>
            <Row style={styles.row}>
              <Col size={25} style={styles.icon}>
                <PlanMeFont color='#5E686E' size={45} name={icon} />
                <Text style={[styles.category, a.status === 'done' && styles.categoryDone, a.status === 'not' && styles.categoryNot]}>{category}</Text>
              </Col>
              {
                a.status === 'done' ?
                  <Col size={60} style={styles.info}>
                    <Row style={{ marginTop: -15 }}>
                      <Col><Text style={styles.title}>{a.name}</Text></Col>
                    </Row>
                    <Row>
                      <Col size={30}><Text style={[styles.subtitle, a.status === 'done' && { marginTop: 35 }]}>{doText}</Text></Col>
                      <Col size={70}>
                        <Col style={{ width: 170, height: 12, backgroundColor: '#CCCFD1', borderRadius: 5, marginTop: 40, borderWidth: 1, borderColor: '#FFF' }}>
                          <Col style={{ width: bar, height: 12, backgroundColor: '#3EE47B', borderRadius: 5, marginTop: -1.2 }} />
                        </Col>
                      </Col>
                    </Row>
                  </Col>
                  :
                  <Col size={60} style={styles.info}>
                    <Text style={styles.title}>{a.name}</Text>
                    <Text style={styles.subtitle}>{doText}</Text>
                  </Col>
              }
              <Col size={15} style={styles.more}>
                {
                  a.status === 'open' ?
                    <Button transparent style={{ width: 70 }} onPress={Actions.activitie}><Text style={styles.link}>MAIS ></Text></Button>
                    : a.status === 'done' ?
                      <PlanMeFont style={{ color: '#3EE47B', marginTop: -40 }} size={30} name='certo' />
                      :
                      <PlanMeFont style={{ color: '#EC135F' }} size={30} name='cancelar' />
                }
              </Col>
            </Row>
          </Col>
        </Row>
      );
    });

    return (
      <CommomPage title='HOJE' page='activities'>
        <Grid>
          {results}
        </Grid>
      </CommomPage>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    height: Platform.OS === 'ios' ? 80 : 90,
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#DADCDD'
  },
  odd: {
    backgroundColor: '#F2F3F3'
  },
  hour: {
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
    color: '#A9AEB1',
    paddingBottom: 10
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10
  },
  category: {
    backgroundColor: '#046AF0',
    color: '#FFF',
    fontSize: 10,
    lineHeight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    width: 90,
    textAlign: 'center',
    marginTop: 3,
    marginBottom: 3
  },
  categoryDone: {
    backgroundColor: '#3EE47B',
  },
  categoryNot: {
    backgroundColor: '#EC135F'
  },
  info: {
    justifyContent: 'center'
  },
  title: {
    fontFamily: 'Montserrat-Regular',
    justifyContent: 'center',
    fontSize: 25,
    lineHeight: 30,
    marginTop: 10,
    marginLeft: 10,
    color: '#757E84'
  },
  subtitle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
    lineHeight: 20,
    marginTop: 5,
    marginLeft: 10,
    color: '#A0A6A9'
  },
  more: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  link: {
    color: '#B67FCA',
    fontSize: 12,
    marginRight: 5
  }
});

if (Platform.OS === 'android') {
  styles.title.marginTop = 20;
}