import React, { Component } from 'react';
import { StyleSheet, Dimensions, Switch, Platform } from 'react-native';
import { Content, List, Text, ListItem, Button, Grid, Row, Col } from 'native-base';
import BridgeNative from '../controller/BridgeNative';
import PlanMeFont from '../Theme/PlanMeFont';
import CommomPage from './CommomPage';
import { Actions } from 'react-native-mobx';
import { observer } from 'mobx-react/native';

var { height, width } = Dimensions.get('window');

@observer
export default class ConfigPage extends Component {
	constructor() {
		super();
		this.logOut = this.logOut.bind(this);
	}

	logOut() {
		BridgeNative.executeAction('logOut', [], [], 'Auth');
		if (!this.props.store.user) Actions.initial();
	}

	render() {
		let inTime = true;
		let barSize = 180;
		let barColor = inTime ? '#3EE47B' : '#EC135F';
		let barIcon = inTime ? 'sucesso' : 'atencao';

		return (
			<CommomPage title='PERSONALIZAR' page='config'>
				<Row>
					<Col>
						<Content style={styles.progress}>
							<Row style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 40, height: 50 }}>
								<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: -15, color: '#535D64' }}>02 FEV</Text>
								<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: (barSize - 20), color: '#535D64' }}>20 MAR</Text>
								<Text style={{ fontFamily: 'Montserrat-Bold', position: 'absolute', marginTop: -20, marginLeft: 240, color: '#122029' }}>02 ABR</Text>
								<Col style={{ width: 3, height: 15, backgroundColor: '#8C9397', borderRadius: 5, marginTop: -15 }} />
								<Col style={{ width: 3, height: 15, backgroundColor: '#8C9397', borderRadius: 5, marginTop: 10, position: 'absolute', marginLeft: barSize }} />
								<Col style={{ width: 250, height: 5, backgroundColor: barColor, marginTop: -15 }}>
									<Col style={{ width: barSize, height: 5, backgroundColor: '#8C9397' }} />
								</Col>
								<PlanMeFont size={22} color={barColor} style={{ marginTop: -13, marginLeft: -2 }} name={barIcon} />
								<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 25 : 20, fontSize: 10, marginLeft: -15, color: '#535D64' }}>INÍCIO</Text>
								<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 25 : 20, fontSize: 10, marginLeft: inTime ? (barSize - 8) : 271, color: inTime ? '#535D64' : '#122029' }}>FIM</Text>
								<Text style={{ fontFamily: 'Montserrat-Regular', position: 'absolute', marginTop: Platform.OS === 'ios' ? 25 : 20, fontSize: 10, marginLeft: !inTime ? (barSize - 8) : 245, color: !inTime ? '#535D64' : '#122029' }}>PROVA</Text>
							</Row>
						</Content>

						<Row style={styles.options}>
							<Col>
								<List style={styles.list}>
									<ListItem style={styles.listItem}>
										<Grid>
											<Row>
												<Col style={styles.listLeft}><Text style={styles.text}>Meus Dados</Text></Col>
												<Col style={styles.listRight}><PlanMeFont size={20} name='direita' /></Col>
											</Row>
										</Grid>
									</ListItem>
									<ListItem style={styles.listItem}>
										<Grid>
											<Row>
												<Col style={styles.listLeft}><Text style={styles.text}>Disciplinas</Text></Col>
												<Col style={styles.listRight}><PlanMeFont size={20} name='direita' /></Col>
											</Row>
										</Grid>
									</ListItem>
									<ListItem style={styles.listItem}>
										<Grid>
											<Row>
												<Col style={styles.listLeft}><Text style={styles.text}>Média de Leitura</Text></Col>
												<Col style={styles.listRight}><Text style={styles.text}>15 p/s</Text></Col>
											</Row>
										</Grid>
									</ListItem>
									<ListItem style={styles.listItem} onPress={Actions.time}>
										<Grid>
											<Row>
												<Col style={styles.listLeft}><Text style={styles.text}>Tempo</Text></Col>
												<Col style={styles.listRight}><PlanMeFont size={20} name='direita' /></Col>
											</Row>
										</Grid>
									</ListItem>
									<ListItem style={styles.listItem}>
										<Grid>
											<Row>
												<Col style={styles.listLeft}><Text style={styles.text}>Revisão</Text></Col>
												<Switch />
											</Row>
										</Grid>
									</ListItem>
								</List>
							</Col>
						</Row>

						<Row style={{ paddingBottom: 20 }}>
							<Col>
								<Row style={styles.replan}>
									<Button style={styles.buttom}><Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>REPLANEJAR</Text></Button>
								</Row>
								<Row style={styles.replan}>
									<Button style={styles.buttom} onPress={this.logOut}><Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>SAIR</Text></Button>
								</Row>
							</Col>
						</Row>
					</Col>
				</Row>
			</CommomPage>
		);
	}
}

const styles = StyleSheet.create({
	progress: {
		height: 100,
		borderBottomColor: '#9AA0A4',
		borderBottomWidth: 1
	},
	options: {
		backgroundColor: '#F2F3F3'
	},
	replan: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	list: {
		backgroundColor: '#FFF',
		marginTop: 25,
		marginBottom: 25,
		borderBottomColor: '#9AA0A4',
		borderBottomWidth: 1
	},
	listItem: {
		height: 60
	},
	listLeft: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center'
	},
	listRight: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		alignItems: 'center'
	},
	buttom: {
		backgroundColor: '#3EE47B',
		width: width - 50,
		marginTop: 30,
		borderRadius: 5
	},
	text: {
		fontFamily: 'Montserrat-Regular'
	}
});