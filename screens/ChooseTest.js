import React, { Component } from 'react';
import { Text, StyleSheet, Image, Dimensions, TextInput, TouchableOpacity, View } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Header, Button, Title, Input, InputGroup, Container, Content, Footer } from 'native-base';
import { Actions } from 'react-native-mobx';

var { height, width } = Dimensions.get('window');

export default class StartAccount extends Component {
	constructor() {
		super();
		this.state = {
			inputGrBack1: '',
			inputGrPl1: 'NOME DA PROVA',
			inputGr1ColorHolder: '#046af0'
		}
		this.validate = this.validate.bind(this);
	}

	componentWillMount() {
		this.state.inputGrBack1 = style.inputGrBack;
		this.state.inputGrBack2 = style.inputGrBack;
		this.state.inputGrBack3 = style.inputGrBack;
	}

	validate() {
		let ckecked = '';
		if (!this.refs.nome._root._lastNativeText) {
			this.state.inputGrBack1 = style.inputGrBackErr;
			this.state.inputGrPl1 = 'OBRIGATÓRIO!';
			this.state.inputGr1ColorHolder = '#ff0000';
		} else {
			ckecked = true;
		}
		if (!ckecked) this.forceUpdate();
		else {
			Actions.chooseTest2();
		}
	}

	render() {
		return (
			<Container>
				<Header style={style.header}>
					<Image style={style.img} source={require('../image/logo.png')} />
				</Header>
				<Content style={style.content}>
					<Text style={style.subtitle}>Qual concurso é este?</Text>
					<Row style={style.inputGr}>
						<InputGroup style={this.state.inputGrBack1} borderType='regular'>
							<Input ref='nome' placeholder={this.state.inputGrPl1} placeholderTextColor={this.state.inputGr1ColorHolder} />
						</InputGroup>
					</Row>
				</Content>
				<Footer style={{ backgroundColor: 'transparent', height: 80, paddingTop: 10 }}>
					<Grid style={{ top: -40 }}>
						<Col>
							<Row style={[style.arrow, { marginRight: 1.5 }]}>
								<View style={style.triangleLeft}></View>
								<TouchableOpacity style={[style.buttom, { backgroundColor: '#CCCFD1', left: -7 }]} onPress={Actions.chooseChallenge}>
									<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>VOLTAR</Text>
								</TouchableOpacity>
							</Row>
						</Col>
						<Col>
							<Row style={[style.arrow, { marginLeft: 1.5 }]}>
								<TouchableOpacity style={[style.buttom, { left: 7 }]} onPress={this.validate}>
									<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>AVANÇAR</Text>
								</TouchableOpacity>
								<View style={style.triangleRight}></View>
							</Row>
						</Col>
					</Grid>
				</Footer>
			</Container>
		);
	}
}

const style = StyleSheet.create({
	header: {
		backgroundColor: '#fff',
		justifyContent: 'center'
	},
	img: {
		width: 80,
		height: 43
	},
	content: { backgroundColor: '#fff' },
	subtitle: {
		textAlign: 'left',
		flexWrap: 'wrap',
		lineHeight: 30,
		fontSize: 20,
		color: '#9D53B8',
		paddingLeft: 35,
		fontFamily: 'Montserrat-Regular',
		marginTop: 30
	},
	inputGr: {
		paddingLeft: 25,
		marginTop: 20,
		flexWrap: 'wrap',
		flexDirection: 'column',
	},
	inputGrBack: {
		backgroundColor: '#fff',
		width: width - 50,
		marginBottom: 20,
		borderRadius: 5
	},
	inputGrBackErr: {
		backgroundColor: '#fff',
		width: width - 50,
		marginBottom: 20,
		borderRadius: 5,
		borderColor: '#ff0000',
		borderWidth: 1.5
	},
	inputGrBackSucess: {
		backgroundColor: '#fff',
		width: width - 50,
		marginBottom: 20,
		borderRadius: 5,
		borderColor: '#32cd32',
		borderWidth: 1.5
	},
	buttom: {
		flex: 1,
		backgroundColor: '#3EE47B',
		marginTop: 40,
		borderRadius: 5,
		height: 46,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	triangleLeft: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 22,
		borderRightWidth: 22,
		borderBottomWidth: 20,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#CCCFD1',
		transform: [{ rotate: '-90deg' }],
		top: 53,
		left: 6
	},
	triangleRight: {
		width: 0,
		height: 0,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 22,
		borderRightWidth: 22,
		borderBottomWidth: 20,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderBottomColor: '#3EE47B',
		transform: [{ rotate: '90deg' }],
		top: 53,
		right: 6
	},
	arrow: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center'
	}
});