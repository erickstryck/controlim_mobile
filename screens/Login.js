import React, { Component } from 'react';
import { StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Container, Content, Text, Header, Button, Title, Input, InputGroup } from 'native-base';
import { Actions } from 'react-native-mobx';
import SnackBar from 'react-native-snackbar-dialog';
import BridgeNative from '../controller/BridgeNative';
import ModalLoad from './ModalLoad';
import { observer } from 'mobx-react/native';

var { height, width } = Dimensions.get('window');

@observer
export default class Login extends Component {
	constructor() {
		super();
		this.state = {
			inputGrBack1: '',
			inputGrBack2: '',
			inputGrBack3: '',
			inputGrPl2: 'E-MAIL',
			inputGrPl3: 'SENHA',
			inputGr1ColorHolder: '#046af0',
			inputGr2ColorHolder: '#046af0',
			inputGr3ColorHolder: '#046af0'
		}
		this.validate = this.validate.bind(this);
	}

	componentWillMount() {
		this.state.inputGrBack1 = style.inputGrBack;
		this.state.inputGrBack2 = style.inputGrBack;
		this.state.inputGrBack3 = style.inputGrBack;
		this.recoverMailValidator = this.recoverMailValidator.bind(this);
		if (!this.props.store.error && this.props.store.user) Actions.progress();
	}

	recoverMailValidator() {
		var ckecked = true;
		if (!this.refs.mail._root._lastNativeText) {
			this.state.inputGrBack2 = style.inputGrBackErr;
			this.state.inputGrPl2 = 'INSIRA E-MAIL DE RECUPERAÇÃO!';
			ckecked = '';
			this.setState(
				{ inputGr2ColorHolder: '#ff0000' }
			);
		} else {
			ckecked = ckecked ? true : '';
			this.state.inputGrBack2 = style.inputGrBackSucess;
			this.state.inputGrPl2 = 'E-MAIL';
			this.setState(
				{ inputGr2ColorHolder: '#046af0' }
			);
		}
		if (ckecked) {
			if (BridgeNative.executeAction('sendPasswordReset', [this.refs.mail._root._lastNativeText], [], 'Auth')) {
				BridgeNative.processError('Um email de recuperação foi enviado para ' + this.refs.mail._root._lastNativeText);
			}
		}
	}

	validate() {
		let ckecked = true;
		if (!this.refs.mail._root._lastNativeText) {
			this.state.inputGrBack2 = style.inputGrBackErr;
			this.state.inputGrPl2 = 'OBRIGATÓRIO!';
			ckecked = '';
			this.state.inputGr2ColorHolder = '#ff0000';
		} else {
			ckecked = ckecked ? true : '';
			this.state.inputGrBack2 = style.inputGrBackSucess;
			this.state.inputGrPl2 = 'E-MAIL';
			this.state.inputGr2ColorHolder = '#046af0';
		}
		if (!this.refs.password._root._lastNativeText) {
			this.state.inputGrBack3 = style.inputGrBackErr;
			this.state.inputGrPl3 = 'OBRIGATÓRIO!';
			ckecked = '';
			this.state.inputGr3ColorHolder = '#ff0000';
		} else {
			ckecked = ckecked ? true : '';
			this.state.inputGrBack3 = style.inputGrBackSucess;
			this.state.inputGrPl3 = 'SENHA!';
			this.state.inputGr3ColorHolder = '#046af0';
		}
		if (!ckecked) this.forceUpdate();
		else {
			BridgeNative.executeAction('toggleSignIn', [this.refs.mail._root._lastNativeText, this.refs.password._root._lastNativeText], [], 'Auth');
		}
	}

	render() {
		return (<Container>
			<Header style={style.header}>
				<Image style={style.img} source={require('../image/logo.png')} />
			</Header>
			<Content style={style.content}>
				<ModalLoad />
				<Text style={style.title}>O desafio{'\n'}está lançado!</Text>
				<Row style={style.inputGr}>
					<InputGroup style={this.state.inputGrBack2} borderType='regular'>
						<Input ref='mail' keyboardType='email-address' placeholder={this.state.inputGrPl2} placeholderTextColor={this.state.inputGr2ColorHolder} />
					</InputGroup>
					<InputGroup style={this.state.inputGrBack3} borderType='regular'>
						<Input ref='password' keyboardType='ascii-capable' secureTextEntry={true} placeholder={this.state.inputGrPl3} placeholderTextColor={this.state.inputGr3ColorHolder} />
					</InputGroup>
					<Text onPress={this.recoverMailValidator} style={[style.textLink, style.forgotPass]}>Esqueci minha senha</Text>
					<Button style={[style.buttom, { marginTop: 50 }]} onPress={this.validate}>
						<Text style={{ color: '#046AF0', fontFamily: 'Montserrat-Bold' }}>LOGIN</Text>
					</Button>
					<Row style={style.link}>
						<Text onPress={Actions.start} style={style.textLink}>CADASTRO</Text>
					</Row>
					<Row style={style.link}>
						<Text onPress={Actions.initial} style={style.textLink}>VOLTAR</Text>
					</Row>
				</Row>
			</Content>
		</Container>);
	}
}

const style = StyleSheet.create({
	header: {
		backgroundColor: '#fff',
		justifyContent: 'center'
	},
	img: {
		width: 80,
		height: 43
	},
	content: { backgroundColor: '#046af0' },
	title: {
		textAlign: 'left',
		flexWrap: 'wrap',
		lineHeight: 40,
		fontSize: 35,
		paddingTop: 30,
		padding: 15,
		paddingLeft: 35,
		color: '#FFF',
		fontFamily: 'Montserrat-Bold'
	},
	subtitle: {
		justifyContent: 'flex-start',
		textAlign: 'left',
		flexWrap: 'wrap',
		lineHeight: 30,
		fontSize: 20,
		color: '#3EE47B',
		paddingLeft: 35,
		fontFamily: 'Montserrat-Light'
	},
	inputGr: {
		paddingLeft: 25,
		marginTop: 40,
		flexWrap: 'wrap',
		flexDirection: 'column'
	},
	inputGrBack: {
		backgroundColor: '#fff',
		width: width - 50,
		marginBottom: 20,
		borderRadius: 5
	},
	inputGrBackErr: {
		backgroundColor: '#fff',
		width: width - 50,
		marginBottom: 20,
		borderRadius: 5,
		borderColor: '#ff0000',
		borderWidth: 1.5
	},
	inputGrBackSucess: {
		backgroundColor: '#fff',
		width: width - 50,
		marginBottom: 20,
		borderRadius: 5,
		borderColor: '#32cd32',
		borderWidth: 1.5
	},
	buttom: {
		backgroundColor: '#3EE47B',
		width: width - 50,
		marginTop: 6,
		borderRadius: 5
	},
	link: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		width: width - 50,
		marginTop: 20
	},
	textLink: {
		color: '#FFF',
		fontFamily: 'Montserrat-Bold',
		textAlign: 'center',
		textDecorationLine: 'underline'
	},
	forgotPass: {
		marginTop: -20,
		textAlign: 'right',
		marginRight: 25,
		fontSize: 12
	}
});