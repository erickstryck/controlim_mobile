import React, { Component } from 'react';
import { StyleSheet, StatusBar, TouchableOpacity } from 'react-native';
import { Container, Header, Title, Content, Footer, Row, Col, Button, Text } from 'native-base';
import PlanMeFont from '../Theme/PlanMeFont';
import myTheme from '../Theme/myTheme.js';

import { Actions } from 'react-native-mobx';

export default class CommmonPage extends Component {

  constructor() {
    super();
    this.state = { logedin: true }
    StatusBar.setBarStyle('light-content', true);
  }

  render() {
    return (
      <Container theme={myTheme}>
        <Header style={styles.header}>
          {this.props.leftButton ? this.props.leftButton : <Button transparent><Text /></Button>}
          <Title style={styles.headerTitle}>{this.props.title || ''}</Title>
          {this.props.rightButton ? this.props.rightButton : <Button transparent><Text /></Button>}
        </Header>

        <Content>
          {this.props.children}
        </Content>

        <Footer>
          <Row style={styles.footer}>
            <Col style={[styles.footerTab, this.props.page === 'progress' && styles.activeTitle]}>
              <TouchableOpacity onPress={this.props.page !== 'progress' ? Actions.progress : () => { }}>
                <Col style={styles.footerTab}>
                  <PlanMeFont style={styles.footerIcon} size={25} name='progresso' />
                  <Title style={styles.footerText}>PROGRESSO</Title>
                </Col>
              </TouchableOpacity>
            </Col>

            <Col style={[styles.footerTab, this.props.page === 'activities' && styles.activeTitle]}>
              <TouchableOpacity onPress={this.props.page !== 'activities' ? Actions.activities : () => { }}>
                <Col style={styles.footerTab}>
                  <PlanMeFont style={styles.footerIcon} size={25} name='atividade' />
                  <Title style={styles.footerText}>ATIVIDADES</Title>
                </Col>
              </TouchableOpacity>
            </Col>

            <Col style={[styles.footerTab, this.props.page === 'calendar' && styles.activeTitle]}>
              <TouchableOpacity onPress={this.props.page !== 'calendar' ? Actions.calendar : () => { }}>
                <Col style={styles.footerTab}>
                  <PlanMeFont style={styles.footerIcon} size={25} name='agenda' />
                  <Title style={styles.footerText}>AGENDA</Title>
                </Col>
              </TouchableOpacity>
            </Col>

            <Col style={[styles.footerTab, this.props.page === 'config' && styles.activeTitle]}>
              <TouchableOpacity onPress={this.props.page !== 'config' ? Actions.config : () => { }} activeOpacity={2}>
                <Col style={styles.footerTab}>
                  <PlanMeFont style={styles.footerIcon} size={25} name='configuracao' />
                  <Title style={styles.footerText}>CONFIG.</Title>
                </Col>
              </TouchableOpacity>
            </Col>
          </Row>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#046AF0'
  },
  headerTitle: {
    fontFamily: 'Montserrat-Regular',
    fontWeight: '400',
    color: 'white'
  },
  footer: {
    backgroundColor: '#4C575E',
    height: 60,
    justifyContent: 'space-around'
  },
  footerTab: {
    alignItems: 'center'
  },
  footerText: {
    fontFamily: 'Montserrat-Regular',
    fontWeight: '200',
    color: 'white',
    fontSize: 10,
    marginTop: -5,
    flexDirection: 'column'
  },
  footerIcon: {
    marginTop: 5,
    color: '#3EE47B'
  },
  activeTitle: {
    backgroundColor: '#046AF0'
  }
});