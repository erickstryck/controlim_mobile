import Persist from './Persist';
import Store from '../common/Store';

var store = Store.getInstance();
var firebase = '';
var database = '';

//nome, descrição, numero de paginas, author

export default class Controllers extends Persist {

  constructor() {
    super();
    store = Store.getInstance();
    firebase = store.getFirebase();
    database = firebase.database();
  }

  insertUser() {
    var temp = {
      configuracoesTempo: store.configuracoesTempo,
      dataProva: store.dataProva,
      cargoSelecionado: store.cargoSelecionado.id
    }
    var obj = {
      path: 'users/' + store.user.uid,
      val: temp
    }
    this.genericDAO(obj);
  }

  getOnce(data) {
    this.getOnceValue(data);
  }

  update(data) {
    this.updateModel(data);
  }
}