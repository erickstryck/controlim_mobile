import Controller from './Controllers';
import Auth from './Auth';
import Persist from './Persist';
import SnackBar from 'react-native-snackbar-dialog';

export default class BridgeNative {

  //controla todos os erros e ecuta a ação
  static executeAction(method, param = [], constructor = [], auth) {
    try {
      let ret = '';
      switch (auth) {
        case 'Auth':
          ret = BridgeNative.process(Auth, method, param, constructor);
          break;
        case 'Controller':
          ret = BridgeNative.process(Controller, method, param, constructor);
          break;
      }
    } catch (e) {
      BridgeNative.processError(typeof e === 'object' ? e.message : e);
      console.log(e);
    }
  }

  //usa a reflexao para intanciar e executar uma ação
  static process(invoke, meth, param, constructor) {
    constructor.push(this);
    let classe = Reflect.construct(invoke, constructor);
    if (Reflect.has(classe, meth)) {
      return Reflect.apply(classe[meth], classe, param);
    } else throw 'O método ' + meth + ' não foi encontrado!';
  }

  static processError(e) {
    SnackBar.show(e, {
      backgroundColor: '#9D53B8',
      buttonColor: '#000',
      duration: 5000
    });
    console.log('error ', e)
  }
}