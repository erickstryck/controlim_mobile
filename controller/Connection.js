import { NetInfo } from 'react-native';
import Store from '../common/Store';
import * as firebase from 'firebase';
import BridgeNative from './BridgeNative';

var store = Store.getInstance();
var instance = '';
var init = false;

var connectedRef = firebase.database().ref('.info/connected');
connectedRef.on('value', function (snap) {
  if (snap.val() === true) {
    console.log('firebase online!')
  } else {
    if (init) {
      BridgeNative.processError('Impossivel estabelecer uma conexão com a internet, iremos trabalhar em modo off-line!');
    }
    if (!init) init = true;
  }
});

export default class Connection {
  //checa se a conexao está ativa
  static getInstance() {
    if (instance) return instance;
    else {
      instance = new Connection();
      return instance;
    }
  }

  checkConnection() {
    NetInfo.isConnected.fetch().then(isConnected => {
      store.online = isConnected ? true : '';
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      if (!isConnected) BridgeNative.processError('Seu dispositivo não está conectado a uma rede de dados ou wi-fi, o aplicativo está em modo off-line!');
    });
    NetInfo.isConnected.addEventListener(
      'change',
      this.handleFirstConnectivityChange
    );
  }
  //complemento que monitora o estado da conexao
  handleFirstConnectivityChange(isConnected) {
    store.online = isConnected ? true : '';
    console.log('Then, is ' + (isConnected ? 'online' : 'offline'));
    if (!isConnected) BridgeNative.processError('Seu dispositivo não está conectado a uma rede de dados ou wi-fi, o aplicativo está em modo off-line!');
    NetInfo.isConnected.removeEventListener(
      'change',
      this.handleFirstConnectivityChange
    );
  }
  //verifica se a conexao é 3g 4g ou ethernet
  checkConnectionType() {
    NetInfo.fetch().done((reach) => {
      store.connectionType = reach;
      console.log('Initial: ' + reach);
    });
    NetInfo.addEventListener(
      'change',
      this.handleFirstConnectivityTypeChange
    );
  }
  //complemento que monitora o tipo da conexao
  handleFirstConnectivityTypeChange(reach) {
    store.connectionType = reach;
    console.log('First change: ' + reach);
    NetInfo.removeEventListener(
      'change',
      this.handleFirstConnectivityTypeChange
    );
  }
  //verifica se a conexao pode gerar atributos ou consumo excessivo
  ckeckExpensiveConnection() {
    var temp = '';
    NetInfo.isConnectionExpensive()
      .then(isConnectionExpensive => {
        store.connectionExpensive = true;
        console.log('Connection is ' + (isConnectionExpensive ? 'Expensive' : 'Not Expensive'));
      })
      .catch(error => {
        console.error(error);
        temp = error;
      });
    if (temp) throw new Error(temp);
  }
}