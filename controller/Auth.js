import Store from '../common/Store';
import SnackBar from 'react-native-snackbar-dialog';
import BridgeNative from './BridgeNative';

var store = Store.getInstance();

const firebase = store.getFirebase();

export default class Auth {
  //login
  toggleSignIn(email, password) {
    store.modal = true;
    console.log(store);
    if (store.online) {
      if (firebase.auth().currentUser) {
        // START signout
        firebase.auth().signOut();
        // END signout
      } else {
        if (email.length < 4) {
          throw new Error('E-mail inválido');
        }
        if (password.length < 4) {
          throw new Error('Senha inválida');
        }
        // Sign in with email and pass.
        // [START authwithemail]
        if (!email && !password) {
          firebase.auth().signInAnonymously().catch(function (error) {
            // Handle Errors here.
            // [START_EXCLUDE]
            store.error = error.message;
            store.closeModal();
            BridgeNative.processError(error.message);
          });
        } else {
          firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
            // Handle Errors here.
            // [START_EXCLUDE]
            store.error = error.message;
            store.closeModal();
            BridgeNative.processError(error.message);
          });
        }
      }
    } else {
      throw new Error('Não há conexão disponível');
    }
  }
  //cadastro
  handleSignUp(email, password, nome) {
    store.modal = true;
    if (store.online) {
      if (email.length < 4) {
        throw new Error('Email inválido');
      }
      if (password.length < 4) {
        throw new Error('Senha inválida');
      }
      // Sign in with email and pass.
      // [START createwithemail]
      store.temp = nome;
      firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
        // Handle Errors here.
        // [START_EXCLUDE]
        store.error = error.message;
        store.closeModal();
        BridgeNative.processError(error.message);
      });
    } else {
      throw new Error('Não há conexão disponível');
    }
  }
  //envia email de verificação
  sendEmailVerification() {
    store.modal = true;
    if (store.online) {
      var verify = false;
      firebase.auth().currentUser.sendEmailVerification().then(function () {
        verify = true;
      });
      return verify;
    } else {
      throw new Error('Não há conexão disponível');
    }
  }
  //envia email para resetar a senha
  sendPasswordReset(email) {
    store.modal = true;
    if (!email) throw new Error('Email não informado');
    if (store.online) {
      var verify = false;
      firebase.auth().sendPasswordResetEmail(email).then(function () {
        verify = true;
        BridgeNative.processError('Email enviado com sussesso!');
        store.closeModal();
      }).catch(function (error) {
        // Handle Errors here.
        store.error = error.message;
        store.closeModal();
        BridgeNative.processError(error.message);
      });
    } else {
      throw new Error('Não há conexão disponível');
    }
  }

  //login com o facebook 
  facebookLogin() {
    store.modal = true;
    var token = '';
    if (store.online) {
      if (!firebase.auth().currentUser) {
        // [START createprovider]
        var provider = new firebase.auth.FacebookAuthProvider();
        // [END createprovider]
        // [START addscopes]
        provider.addScope('user_birthday');
        // [END addscopes]
        // [START signin]
        firebase.auth().signInWithPopup(provider).then(function (result) {
          // This gives you a Facebook Access Token. You can use it to access the Facebook API.
          token = result.credential.accessToken;
        }).catch(function (error) {
          store.error = error.message;
          store.closeModal();
          BridgeNative.processError(error.message);
        });
        // [END signin]
      } else {
        // [START signout]
        firebase.auth().signOut();
        // [END signout]
      }
    } else {
      throw new Error('Não há conexão disponível');
    }
  }

  logOut() {
    if (firebase.auth().currentUser) {
      firebase.auth().signOut();
      store.user = '';
    }
  }

  insertUser(data) {
    var user = firebase.auth().currentUser;
    user.updateProfile({
      email: data.email,
      emailVerified: data.emailVerified,
      isAnonymous: data.isAnonymous,
      displayName: data.displayName
    }).then(function () {
      console.log('usuário inserido', firebase.auth().currentUser);
    }, function (error) {
      throw new Error('Houve error ao salvar o usuário');
    });
  }

  anonLogin() {
    store.modal = true;
    if (firebase.auth().currentUser) {
      firebase.auth().signOut();
    } else {
      // [START authanon]
      firebase.auth().signInAnonymously().catch(function (error) {
        // Handle Errors here.
        store.error = error.message;
        store.closeModal();
        BridgeNative.processError(error.message);
        // [END_EXCLUDE]
      });
      // [END authanon]
    }
  }
}