import Store from '../common/Store';
var store = "";
var firebase = "";
var database = "";

export default class Persist {
  constructor() {
    store = Store.getInstance();
    firebase = store.getFirebase();
    database = firebase.database();
  }

  genericDAO(data) {
    firebase.database().ref(data.path).set(data.val);
  }

  getOnceValue(data) {
    firebase.database().ref(data.path).once('value').then((snapshot) => {
      store[data.path] = snapshot.val();
    });
  }

  updateModel(data) {
    firebase.database().ref(data.path).update(data.obj);
  }
}