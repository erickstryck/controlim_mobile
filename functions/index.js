var functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.updateUser = functions.https.onRequest((req, res) => {
  admin.auth().getUser(req.body.uid).then(result => {
    if (req.body.uid === result.uid) {
      let db = admin.database();
      db.ref('cargos/' + req.body.cargoId).on('value', (cargo) => {
        let _cargo = cargo.val();
        let disciplinas = _cargo.disciplinas;
        delete _cargo.disciplinas;
        db.ref('users/' + req.body.uid + '/cargoSelecionado').update(_cargo);

        disciplinas.map((d) => {
          db.ref('disciplinas/' + d).on('value', (nome) => {
            let temp = {};
            temp[d] = { nome: nome.val() };
            db.ref('users/' + req.body.uid + '/cargoSelecionado/disciplinas').update(temp);
            db.ref('livros').orderByChild('disciplinaId').equalTo(d).on('child_added', (livro) => {
              let _livro = livro.val();
              delete _livro.disciplinaId;
              let obj = {};
              obj[livro.key] = _livro;
              db.ref('users/' + req.body.uid + '/cargoSelecionado/disciplinas/' + d + '/livros').update(obj);
            });
          });
        });
        res.send(200);
      });
    } else {
      throw new Error('Não foi possivel validar a operação!');
    }
  }).catch(e => {
    res.send({ message: 'deu pau', error: e });
  });
});

exports.getConcursoCargo = functions.https.onRequest((req, res) => {
  admin.auth().getUser(req.body.uid).then(result => {
    if (req.body.uid === result.uid) {
      let db = admin.database();
      db.ref('cargos').once('value').then(cgs => {
        db.ref('concursos').once('value').then(concurs => {
          let cg = cgs.val();
          let conc = concurs.val();
          cg = Object.keys(cg).map(key => {
            delete cg[key].postoId;
            delete cg[key].disciplinas;
            delete cg[key].leis;
            cg[key].concursoId = conc[cg[key].concursoId];
            cg[key].id = key;
            return cg[key];
          });
          res.json(cg);
        });
      });
    } else {
      throw new Error('Não foi possivel validar a operação!');
    }
  }).catch(e => {
    res.send({ message: 'deu pau', error: e });
  });
});