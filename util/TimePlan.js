import moment from 'moment';

class TimePlan {

  constructor() {
    this.cores = { disciplina: '#046AF0', lei: '#9D53B8', revisao: '#EC135F', exercicio: '#EB7A00', informativo: '#3EE47B' };
    this.tempos = {
      '90': { disciplina: 30, lei: 15, exercicio: 15, revisao: 30 },
      '120': { disciplina: 45, lei: 15, exercicio: 30, revisao: 30 },
      '180': { disciplina: 75, lei: 30, exercicio: 30, revisao: 45 },
      '240': { disciplina: 120, lei: 30, exercicio: 30, revisao: 60 },
      '300': { disciplina: 150, lei: 30, exercicio: 30, revisao: 90 },
      '360': { disciplina: 180, lei: 30, exercicio: 45, revisao: 105 },
      '420': { disciplina: 210, lei: 45, exercicio: 45, revisao: 120 },
      '480': { disciplina: 240, lei: 60, exercicio: 60, revisao: 120 },
      '540': { disciplina: 270, lei: 60, exercicio: 60, revisao: 150 },
      '600': { disciplina: 300, lei: 60, exercicio: 60, revisao: 180 },
      '660': { disciplina: 300, lei: 90, exercicio: 90, revisao: 180 },
      '720': { disciplina: 330, lei: 90, exercicio: 90, revisao: 210 },
      '780': { disciplina: 330, lei: 120, exercicio: 120, revisao: 210 },
      '840': { disciplina: 360, lei: 120, exercicio: 120, revisao: 240 },
      '900': { disciplina: 390, lei: 120, exercicio: 120, revisao: 270 },
    }
  }

  calcularTempo(configuracaoTempo) {

    let data = [
      { dia: 'D', horarios: [] },
      { dia: 'S', horarios: [] },
      { dia: 'T', horarios: [] },
      { dia: 'Q', horarios: [] },
      { dia: 'Q', horarios: [] },
      { dia: 'S', horarios: [] },
      { dia: 'S', horarios: [] }
    ];
    let inicio = '08:00';
    Object.keys(configuracaoTempo).map((c, k) => {
      let grade = this.tempos[configuracaoTempo[c].minutosDiarios];
      let atividade = { hora: inicio, cor: '', duration: 0 };
      let result = [];
      let tempoTotal = 0;

      Object.keys(grade).map((key) => {
        let pieces = grade[key] / 15;
        atividade.cor = configuracaoTempo[c].checked ? this.cores[key] : '#DAD4C2';

        for (let i = 0; i < pieces; i++) {
          if (tempoTotal + 15 > 120) {
            result.push(JSON.parse(JSON.stringify(atividade)));
            atividade.hora = moment(atividade.hora, 'HH:mm').add(atividade.duration + 15, 'm').format('HH:mm');
            atividade.duration = 0;
            tempoTotal = 0;
          }
          atividade.duration += 15;
          tempoTotal += 15;
        }
        if (tempoTotal > 0) {
          result.push(JSON.parse(JSON.stringify(atividade)));
          atividade.hora = moment(atividade.hora, 'HH:mm').add(atividade.duration, 'm').format('HH:mm');
          atividade.duration = 0;
        }
      });
      data[k].horarios = result;
    });
    return data;
  }
}

export default new TimePlan();