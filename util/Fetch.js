import Store from "../common/Store";
export default class Fetch {

  static comunicate(target, type, bdy, path) {
    var store = Store.getInstance();
    store.modal = true;
    fetch(target, {
      method: type,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(bdy)
    })
    .then(response => response.json())
    .then(response => {
      store.inputData(path, response);
    }).catch((error) => {
      throw new Error(error);
    });
  }

}