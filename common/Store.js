import { observable } from 'mobx';
import autobind from 'autobind-decorator'
import * as firebase from 'firebase';
import { AsyncStorage } from 'react-native';
import moment from 'moment';

var instance = ''; //usado para variaveis temporarias
@autobind
export default class Store {

	@observable temp = '';
	@observable user = firebase.auth().currentUser;
	@observable modal = false;
	@observable disciplinas = '';
	@observable disciplinasSelecionadas = [];
	@observable dataProva = '';
	@observable cargoSelecionado = 'ESCOLHA UMA PROVA';
	@observable online = '';
	@observable connectionType = '';
	@observable connectionExpensive = '';
	@observable error = '';
	@observable concursos = {};
	@observable configuracoesTempo = {
		Do: { checked: true, minutosDiarios: 480 },
		Se: { checked: true, minutosDiarios: 480 },
		Te: { checked: true, minutosDiarios: 480 },
		Qa: { checked: true, minutosDiarios: 480 },
		Qi: { checked: true, minutosDiarios: 480 },
		Sx: { checked: true, minutosDiarios: 480 },
		Sa: { checked: true, minutosDiarios: 480 },
	};

	constructor() {
		//Api que configura o firebase
		var config = {
			apiKey: 'AIzaSyBQAaZM-LpxaXXUE0DcWgdojyCQiC0282I',
			authDomain: 'testefirebase-77a03.firebaseapp.com',
			databaseURL: 'https://testefirebase-77a03.firebaseio.com',
			storageBucket: 'testefirebase-77a03.appspot.com',
			messagingSenderId: '983686485067'
		};
		firebase.initializeApp(config);
		//escuta mudanças no estado do usuário e seta na store
		firebase.auth().onAuthStateChanged(function (userFB) {
			if (userFB) {
				Store.storageGet('user').then(res => {
					if (!res) {
						var user = {
							'email': userFB.email,
							'emailVerified': userFB.emailVerified,
							'isAnonymous': userFB.isAnonymous,
							'displayName': instance.temp,
							'uid': userFB.uid,
							'init': moment().add(7, 'd')
						}
						instance.alterUser(user);
						instance.closeModal();
						instance.inputData('user', user);
					}
				});
			}
		});
	}

	async inputData(key, val) {
		this[key] = val;
		if (val instanceof Object) val = JSON.stringify(val);
		else val = JSON.stringify({ 'obj': val });
		let obj = await Store.storageGet(key);
		if (obj && key) {
			Store.storageMerge(key, val);
		} else if (key) Store.storageSet(key, val);
	}

	static async storageGet(key) {
		try {
			const value = await AsyncStorage.getItem(key);
			if (value !== null) {
				return value;
			} else throw new Error('Value empty!');
		} catch (error) {
			console.log(error);
		}
	}

	static async storageSet(key, value) {
		try {
			await AsyncStorage.setItem(key, value);
		} catch (error) {
			console.log(error);
		}
	}

	static async storageRemove(key) {
		try {
			await AsyncStorage.removeItem(key, value);
		} catch (error) {
			console.log(error);
		}
	}

	static async storageMerge(key, value) {
		console.log(key, value)
		try {
			await AsyncStorage.mergeItem(key, value);
		} catch (error) {
			console.log(error);
		}
	}

	static async getKeys() {
		try {
			return await AsyncStorage.getAllKeys();
		} catch (error) {
			console.log(error);
		}
	}

	static clearStorage() {
		AsyncStorage.clear();
	}

	static getInstance() {
		if (instance) return instance;
		else {
			instance = new Store();
			return instance;
		}
	}

	alterUser(info) {
		this.user = info;
	}

	closeModal(time = 1000) {
		setTimeout(() => this.modal = false, time);
	}

	getFirebase() {
		return firebase;
	}
};

