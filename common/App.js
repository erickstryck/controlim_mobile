import React, { Component } from 'react';
import { observable } from 'mobx';
import { Actions, ActionConst, Router, Scene, Switch } from 'react-native-mobx';
import { observer } from 'mobx-react/native';
import Store from './Store';
import Login from '../screens/Login';
import Initial from '../screens/Initial';
import StartAccount from '../screens/StartAccount';
import ChooseChallenge from '../screens/ChooseChallenge';
import SearchTest from '../screens/SearchTest';
import ChooseTest from '../screens/ChooseTest';
import ChooseTestP2 from '../screens/ChooseTestP2';
import ChooseTestP3 from '../screens/ChooseTestP3';
import ResumePlan from '../screens/ResumePlan';
import AdjustTime from '../screens/AdjustTime';
import moment from 'moment';
import ProgressPage from '../screens/ProgressPage';
import ActivitiesPage from '../screens/ActivitiesPage';
import ActivitiePage from '../screens/ActivitiePage';
import CalendarPage from '../screens/CalendarPage';
import AddEvent from '../screens/AddEvent';
import ConfigPage from '../screens/ConfigPage';
import EditTime from '../screens/EditTime';
import Connection from '../controller/Connection';

Connection.getInstance().checkConnection();

const scenes = Actions.create(
  <Scene key='root' type={ActionConst.RESET} tabs={true} >
    <Scene hideNavBar={true} key='initial' component={observer(Initial)} />
    <Scene hideNavBar={true} key='login' component={observer(Login)} />
    <Scene hideNavBar={true} key='start' component={observer(StartAccount)} />
    <Scene hideNavBar={true} key='chooseChallenge' component={observer(ChooseChallenge)} />
    <Scene hideNavBar={true} key='searchTest' component={observer(SearchTest)} />
    <Scene hideNavBar={true} key='chooseTest' component={observer(ChooseTest)} />
    <Scene hideNavBar={true} key='chooseTest2' component={observer(ChooseTestP2)} />
    <Scene hideNavBar={true} key='chooseTest3' component={observer(ChooseTestP3)} />
    <Scene hideNavBar={true} key='resumePlan' component={observer(ResumePlan)} />
    <Scene hideNavBar={true} key='adjustTime' component={observer(AdjustTime)} />
    <Scene hideNavBar={true} key='progress' component={observer(ProgressPage)} />
    <Scene hideNavBar={true} key='activities' component={observer(ActivitiesPage)} />
    <Scene hideNavBar={true} key='activitie' component={observer(ActivitiePage)} />
    <Scene hideNavBar={true} key='calendar' component={observer(CalendarPage)} />
    <Scene hideNavBar={true} key='addEvent' component={observer(AddEvent)} />
    <Scene hideNavBar={true} key='config' component={observer(ConfigPage)} />
    <Scene hideNavBar={true} key='time' component={observer(EditTime)} />
  </Scene>
);

var store = Store.getInstance();

@observer
export default class App extends Component {

  componentWillMount() {
    store.user = "";
    Object.keys(store).map((key, idx) => {
      Store.storageGet(key).then(value => {
        store.modal = true;
        if (value) store[key] = JSON.parse(value).obj ? JSON.parse(value).obj : JSON.parse(value);
        if (idx == Object.keys(store).length - 1) {
          let now = moment().unix();
          if (moment().unix() > parseInt(store.user.init)) {
            console.log("tem que pagar");
          } else {
            store.closeModal();
          }
        }
      });
    });
  }

  render() {
    return (
      <Router store={Store.getInstance()} scenes={scenes} />
    );
  }
}