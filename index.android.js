import { AppRegistry } from 'react-native';

import App from './common/App';

AppRegistry.registerComponent('PlanMe', () => App);