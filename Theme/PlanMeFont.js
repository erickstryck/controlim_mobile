import createIconSet from './createIconSet';
import glyphMap from './glyphMap.json';

const iconSet = createIconSet(glyphMap, 'planme', 'planme.ttf');

export default iconSet;